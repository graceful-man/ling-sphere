package com.abin.mallchat.oss;

import io.minio.MinioClient;
import lombok.SneakyThrows;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 作者：fsh
 * 日期：2024/03/16
 * <p>
 * 描述： minio 配置类
 */
// 这是一个配置类，使用@Configuration注解标记，其中proxyBeanMethods = false表示不代理bean方法，提高性能
@Configuration(proxyBeanMethods = false)
// 启用配置属性绑定，将OssProperties类的属性绑定到配置文件中的属性
@EnableConfigurationProperties(OssProperties.class)
// 根据表达式${oss.enabled}的值来决定是否创建此配置类中的bean，只有当oss.enabled为true时才会创建
@ConditionalOnExpression("${oss.enabled}")
// 根据oss.type的值来决定是否创建此配置类中的bean，只有当oss.type等于"minio"时才会创建
@ConditionalOnProperty(value = "oss.type", havingValue = "minio")
public class MinIOConfiguration {


    @Bean// 定义一个Bean，当没有MinioClient的Bean存在时，会创建这个Bean
    @SneakyThrows// 使用Lombok的@SneakyThrows注解来避免在方法上显式地抛出异常
    @ConditionalOnMissingBean(MinioClient.class) // 当存在 MinioClient字节码的时候
    public MinioClient minioClient(OssProperties ossProperties) {
        // 使用MinioClient.builder()创建一个MinioClient的实例，并设置其属性
        // 使用OssProperties对象获取相关的配置信息
        return MinioClient.builder()
                .endpoint(ossProperties.getEndpoint())  // 设置MinIO服务的地址
                .credentials(ossProperties.getAccessKey(), ossProperties.getSecretKey())  // 设置访问MinIO服务的凭证
                .build();  // 构建并返回MinioClient对象
    }

    // 定义另一个Bean，当存在MinioClient的Bean并且没有MinIOTemplate的Bean时，会创建这个Bean
    @Bean
    @ConditionalOnBean({MinioClient.class})
    @ConditionalOnMissingBean(MinIOTemplate.class)
    public MinIOTemplate minioTemplate(MinioClient minioClient, OssProperties ossProperties) {
        // 使用已存在的MinioClient对象和OssProperties对象来创建一个MinIOTemplate对象，并返回
        return new MinIOTemplate(minioClient, ossProperties);
    }
}