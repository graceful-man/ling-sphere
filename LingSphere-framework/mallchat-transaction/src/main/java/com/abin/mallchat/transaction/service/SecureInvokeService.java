package com.abin.mallchat.transaction.service;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ReflectUtil;
import cn.hutool.extra.spring.SpringUtil;
import com.abin.mallchat.transaction.dao.SecureInvokeRecordDao;
import com.abin.mallchat.transaction.domain.dto.SecureInvokeDTO;
import com.abin.mallchat.transaction.domain.entity.SecureInvokeRecord;
import com.fasterxml.jackson.databind.JsonNode;
import com.fsh.lingsphere.utils.JsonUtils;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.transaction.support.TransactionSynchronization;
import org.springframework.transaction.support.TransactionSynchronizationManager;

import javax.validation.constraints.NotNull;
import java.lang.reflect.Method;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.stream.Collectors;

/**
 * Description: 安全执行处理器
 * Author: <a href="https://github.com/zongzibinbin">abin</a>
 * Date: 2023-08-20
 */
@Slf4j
@AllArgsConstructor
public class SecureInvokeService {

    public static final double RETRY_INTERVAL_MINUTES = 2D;


    /**
     * Q：这里为什么  secureInvokeRecordDao、executor  没有注入，却还是能够使用？
     * A：没有自动注入的话，可以看看是不是构造注入！ 类上加了 @AllArgsConstructor  注解，是构造注入！
     * 具体在 config中的 TransactionAutoConfiguration 类中。
     */
    private final SecureInvokeRecordDao secureInvokeRecordDao;
    private final Executor executor;


    public void invoke(SecureInvokeRecord record, boolean async) {
        boolean inTransaction = TransactionSynchronizationManager.isActualTransactionActive();
        //非事务状态，直接执行原来被拦截的方法，不做任何保证。
        if (!inTransaction) {
            return;
        }

        //将操作记录插入数据库中secure_invoke_record表，保存执行数据。
        secureInvokeRecordDao.save(record);

        //注册一个事务同步对象，该对象将在事务提交后执行一些操作。
        //我们的操作记录入库后，就立即先执行一次MQ发送等后续。 不会等到定时任务来做。
        TransactionSynchronizationManager.registerSynchronization(new TransactionSynchronization() {
            @SneakyThrows
            @Override
            public void afterCommit() {
                //事务后执行。异步执行方法调用
                if (async) {
                    doAsyncInvoke(record);
                } else {
                    //同步执行
                    doInvoke(record);
                }
            }
        });
    }

    // 线程池调用 ，异步执行
    private void doAsyncInvoke(SecureInvokeRecord record) {
        executor.execute(() -> {
            System.out.println(Thread.currentThread().getName());
            doInvoke(record);
        });
    }


    private void doInvoke(SecureInvokeRecord record) {
        //获取SecureInvokeDTO对象，其中包含了被拦截方法的详细信息
        SecureInvokeDTO secureInvokeDTO = record.getSecureInvokeDTO();
        try {
            // 标记当前线程为正在执行安全调用
            SecureInvokeHolder.setInvoking();
            // 根据类名获取Class对象
            Class<?> beanClass = Class.forName(secureInvokeDTO.getClassName());
            // 通过Spring工具类获取bean实例
            Object bean = SpringUtil.getBean(beanClass);
            // 将参数类型列表转换成字符串列表
            List<String> parameterStrings = JsonUtils.toList(secureInvokeDTO.getParameterTypes(), String.class);
            // 将字符串形式的参数类型转换成Class对象的列表
            List<Class<?>> parameterClasses = getParameters(parameterStrings);
            // 通过反射获取bean中对应的方法
            Method method = ReflectUtil.getMethod(beanClass, secureInvokeDTO.getMethodName(), parameterClasses.toArray(new Class[]{}));
            // 获取方法调用所需的参数值
            Object[] args = getArgs(secureInvokeDTO, parameterClasses);
            //执行方法 ，执行原来被拦截的方法（发送MQ消息）
            method.invoke(bean, args);
            //执行成功更新操作表中的记录状态（实际上就是删除掉这条记录）
            removeRecord(record.getId());
        } catch (Throwable e) {
            log.error("SecureInvokeService invoke fail", e);
            //记录并更新这次失败记录。执行失败，等待下次执行（有定时任务触发）。
            retryRecord(record, e.getMessage());
        } finally {
            SecureInvokeHolder.invoked();
        }
    }

    // 根据操作记录的id，删除掉记录
    private void removeRecord(Long id) {
        secureInvokeRecordDao.removeById(id);
    }

    // MQ发生异常后 ，重试。 重试记录方法，更新记录的重试次数、失败原因以及下次重试时间。
    private void retryRecord(SecureInvokeRecord record, String errorMsg) {
        // 将重试次数加1
        Integer retryTimes = record.getRetryTimes() + 1;
        // 创建一个新的SecureInvokeRecord对象用于更新
        SecureInvokeRecord update = new SecureInvokeRecord();
        update.setId(record.getId());
        update.setFailReason(errorMsg);
        // 下次重试的时间（此方法中采用了退避算法，具体可以看方法中）
        update.setNextRetryTime(getNextRetryTime(retryTimes));
        //当重试次数 大于 最大次数 ，记录为失败，不再重试
        if (retryTimes > record.getMaxRetryTimes()) {
            update.setStatus(SecureInvokeRecord.STATUS_FAIL);
        } else {
            update.setRetryTimes(retryTimes);
        }
        //更新表，更新操作记录
        secureInvokeRecordDao.updateById(update);
    }


    //可以采用退避算法 ，计算操作失败后 ，下一次重试的具体时间
    private Date getNextRetryTime(Integer retryTimes) {
        double waitMinutes = Math.pow(RETRY_INTERVAL_MINUTES, retryTimes);//重试时间指数上升 2m 4m 8m 16m
        return DateUtil.offsetMinute(new Date(), (int) waitMinutes);
    }

    /**
     *  定时任务，每5秒执行一次。
     *
     *  每5秒钟 拉取操作记录表中失败的数据。重新执行。
     */
    @Scheduled(cron = "*/5 * * * * ?")
    public void retry() {
        // 查找操作记录中失败的数据 （默认只查2分钟前的）
        List<SecureInvokeRecord> secureInvokeRecords = secureInvokeRecordDao.getWaitRetryRecords();
        for (SecureInvokeRecord secureInvokeRecord : secureInvokeRecords) {
            //异步执行
            doAsyncInvoke(secureInvokeRecord);
        }
    }


    @NotNull
    private Object[] getArgs(SecureInvokeDTO secureInvokeDTO, List<Class<?>> parameterClasses) {
        JsonNode jsonNode = JsonUtils.toJsonNode(secureInvokeDTO.getArgs());
        Object[] args = new Object[jsonNode.size()];
        for (int i = 0; i < jsonNode.size(); i++) {
            Class<?> aClass = parameterClasses.get(i);
            args[i] = JsonUtils.nodeToValue(jsonNode.get(i), aClass);
        }
        return args;
    }

    @NotNull
    private List<Class<?>> getParameters(List<String> parameterStrings) {
        return parameterStrings.stream().map(name -> {
            try {
                return Class.forName(name);
            } catch (ClassNotFoundException e) {
                log.error("SecureInvokeService class not fund", e);
            }
            return null;
        }).collect(Collectors.toList());
    }
}
