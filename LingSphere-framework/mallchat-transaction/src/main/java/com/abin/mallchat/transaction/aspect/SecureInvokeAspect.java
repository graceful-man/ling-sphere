package com.abin.mallchat.transaction.aspect;

import cn.hutool.core.date.DateUtil;
import com.abin.mallchat.transaction.annotation.SecureInvoke;
import com.abin.mallchat.transaction.domain.dto.SecureInvokeDTO;
import com.abin.mallchat.transaction.domain.entity.SecureInvokeRecord;
import com.abin.mallchat.transaction.service.SecureInvokeHolder;
import com.abin.mallchat.transaction.service.SecureInvokeService;
import com.fsh.lingsphere.utils.JsonUtils;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.transaction.support.TransactionSynchronizationManager;

import java.lang.reflect.Method;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Description: 安全执行切面
 * Author: <a href="https://github.com/zongzibinbin">abin</a>
 * Date: 2023-04-20
 */
@Slf4j
@Aspect //声明这个类是一个切面类，用于定义切面的行为。
@Order(Ordered.HIGHEST_PRECEDENCE + 1)//确保最先执行
@Component
public class SecureInvokeAspect {

    @Autowired
    private SecureInvokeService secureInvokeService;

    /**
     * 定义一个环绕通知（Around Advice），它会拦截所有标记了@SecureInvoke注解的方法调用。
     */
    @Around("@annotation(secureInvoke)")
    public Object around(ProceedingJoinPoint joinPoint, SecureInvoke secureInvoke) throws Throwable {
        //是否是异步执行
        boolean async = secureInvoke.async();
        //检查当前是否在一个活跃的事务中。
        boolean inTransaction = TransactionSynchronizationManager.isActualTransactionActive();
        //非事务状态，直接执行，不做任何保证。直接执行原来被拦截的方法并返回结果
        if (SecureInvokeHolder.isInvoking() || !inTransaction) {
            return joinPoint.proceed();
        }

        //获取被拦截方法的Method对象。
        Method method = ((MethodSignature) joinPoint.getSignature()).getMethod();
        //获取被拦截方法的参数类型列表，并将其转换为字符串列表。
        List<String> parameters = Stream.of(method.getParameterTypes()).map(Class::getName).collect(Collectors.toList());
        //用建造者模式创建一个SecureInvokeDTO对象。
        SecureInvokeDTO dto = SecureInvokeDTO.builder()
                .args(JsonUtils.toStr(joinPoint.getArgs()))
                .className(method.getDeclaringClass().getName())
                .methodName(method.getName())
                .parameterTypes(JsonUtils.toStr(parameters))
                .build();
        //使用建造者模式创建一个SecureInvokeRecord对象，设置属性，用来插入数据库中
        SecureInvokeRecord record = SecureInvokeRecord.builder()
                .secureInvokeDTO(dto)
                .maxRetryTimes(secureInvoke.maxRetryTimes())
                .nextRetryTime(DateUtil.offsetMinute(new Date(), (int) SecureInvokeService.RETRY_INTERVAL_MINUTES))
                .build();
        //调用方法，传入record和async标志
        secureInvokeService.invoke(record, async);
        return null;
    }
}
