package com.abin.mallchat.transaction.service;

import cn.hutool.core.util.RandomUtil;
import com.abin.mallchat.transaction.annotation.SecureInvoke;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;

/**
 * Description: 发送mq工具类。   MQProducer 在 config包中的TransactionAutoConfiguration 纳入了Bean管理
 * Author: <a href="https://github.com/zongzibinbin">abin</a>
 * Date: 2023-08-12
 */
public class MQProducer {

    @Autowired
    private RocketMQTemplate rocketMQTemplate;

    public void sendMsg(String topic, Object body) {
        Message<Object> build = MessageBuilder.withPayload(body).build();
        rocketMQTemplate.send(topic, build);
    }

    /**
     * 发送可靠消息，在事务提交后保证发送成功
     *
     *  rocketMq发送消息，使用了 @SecureInvoke 注解 ，保证可靠发送。
     *  Q：为什么 @SecureInvoke 注解 保证了安全可靠的发送。
     *  A：因为定义了一个切面类，里面的环绕通知方法拦截了 所有标记了@SecureInvoke注解的方法调用。
     *  在这里有可靠性设计实现。 具体看切面方法。
     *
     * @param topic
     * @param body
     */
    @SecureInvoke
    public void sendSecureMsg(String topic, Object body, Object key) {
//        //模拟MQ失败场景
//        if (RandomUtil.randomInt(3)>1){
//            throw new IllegalArgumentException();
//        }

        Message<Object> build = MessageBuilder
                .withPayload(body)
                .setHeader("KEYS", key)
                .build();
        rocketMQTemplate.send(topic, build);
    }
}
