package com.abin.frequencycontrol.aspect;


import cn.hutool.core.util.StrUtil;
import com.abin.frequencycontrol.constant.FrequencyControlConstant;
import com.abin.frequencycontrol.util.RequestHolder;
import com.abin.frequencycontrol.annotation.FrequencyControl;
import com.abin.frequencycontrol.domain.dto.FixedWindowDTO;
import com.abin.frequencycontrol.domain.dto.FrequencyControlDTO;
import com.abin.frequencycontrol.domain.dto.SlidingWindowDTO;
import com.abin.frequencycontrol.domain.dto.TokenBucketDTO;
import com.abin.frequencycontrol.service.frequencycontrol.FrequencyControlUtil;
import com.fsh.lingsphere.utils.SpElUtils;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 作者：fsh
 * 日期：2024/03/23
 * <p>
 * 描述：
 * 频控实现 的切面类
 */
@Slf4j
@Aspect
@Component
public class FrequencyControlAspect {

    /**
     * 使用@Around注解来标记这是一个环绕通知，拦截标注了FrequencyControl或FrequencyControlContainer注解的方法
     *
     * @param joinPoint 连接点
     * @return {@link Object }
     */
    @Around("@annotation(com.abin.frequencycontrol.annotation.FrequencyControl)||@annotation(com.abin.frequencycontrol.annotation.FrequencyControlContainer)")
    public Object around(ProceedingJoinPoint joinPoint) throws Throwable {
        // 获取被拦截方法的Method对象
        Method method = ((MethodSignature) joinPoint.getSignature()).getMethod();
        // 获取方法上标注的FrequencyControl注解数组
        FrequencyControl[] annotationsByType = method.getAnnotationsByType(FrequencyControl.class);
        // 创建一个HashMap来存储频控注解和它们的key
        Map<String, FrequencyControl> keyMap = new HashMap<>();
        // 初始化策略为总量固定时间窗口
        String strategy = FrequencyControlConstant.TOTAL_COUNT_WITH_IN_FIX_TIME;
        // 遍历 方法上标注的FrequencyControl注解数组
        for (int i = 0; i < annotationsByType.length; i++) {
            // 获取当前遍历到的频控注解
            FrequencyControl frequencyControl = annotationsByType[i];
            // 获取前缀key，如果没有设置则使用默认方法限定名加上注解的排名。    默认方法限定名 + 注解排名（可能多个）
            String prefix = StrUtil.isBlank(frequencyControl.prefixKey()) ? method.toGenericString() + ":index:" + i : frequencyControl.prefixKey();
            String key = "";
            // 根据注解的target字段确定key的生成方式。 拼接Key
            switch (frequencyControl.target()) {
                case EL:
                    // 如果target是EL，则使用SpElUtils解析SpEL表达式来生成key
                    key = SpElUtils.parseSpEl(method, joinPoint.getArgs(), frequencyControl.spEl());
                    break;
                case IP:
                    // 如果target是IP，则获取请求的IP作为key
                    key = RequestHolder.get().getIp();
                    break;
                case UID:
                    // 如果target是UID，则获取请求的UID作为key
                    key = RequestHolder.get().getUid().toString();
            }
            // 将生成的key和对应的频控注解存入keyMap
            keyMap.put(prefix + ":" + key, frequencyControl);
            // 更新策略，循环遍历完后，以最后一个注解的策略为准
            strategy = frequencyControl.strategy();
        }
        // 根据策略进行不同的频控处理
        if (FrequencyControlConstant.TOTAL_COUNT_WITH_IN_FIX_TIME.equals(strategy)) {
            // 如果策略是固定窗口计数
            // 将注解参数转换为固定窗口计数所需的DTO列表
            List<FrequencyControlDTO> frequencyControlDTOS = keyMap.entrySet().stream().map(entrySet ->
                    buildFixedWindowDTO(entrySet.getKey(), entrySet.getValue())
            ).collect(Collectors.toList());
            // 执行带有频控的方法调用
            return FrequencyControlUtil.executeWithFrequencyControlList(strategy, frequencyControlDTOS, joinPoint::proceed);

        } else if (FrequencyControlConstant.TOKEN_BUCKET.equals(strategy)) {
            // 如果策略是令牌桶算法
            // 将注解参数转换为令牌桶算法所需的DTO列表
            List<TokenBucketDTO> frequencyControlDTOS = keyMap.entrySet().stream().map(entrySet ->
                    buildTokenBucketDTO(entrySet.getKey(), entrySet.getValue())
            ).collect(Collectors.toList());
            // 执行带有频控的方法调用
            return FrequencyControlUtil.executeWithFrequencyControlList(strategy, frequencyControlDTOS, joinPoint::proceed);

        } else {
            // 如果策略是滑动窗口算法
            // 将注解参数转换为滑动窗口算法所需的DTO列表
            List<SlidingWindowDTO> frequencyControlDTOS = keyMap.entrySet().stream().map(entrySet ->
                    buildSlidingWindowFrequencyControlDTO(entrySet.getKey(), entrySet.getValue())
            ).collect(Collectors.toList());
            // 执行带有频控的方法调用
            return FrequencyControlUtil.executeWithFrequencyControlList(strategy, frequencyControlDTOS, joinPoint::proceed);
        }
    }

    /**
     * 将注解参数转换为编程式调用所需要的参数
     *
     * @param key              频率控制Key
     * @param frequencyControl 注解
     * @return 编程式调用所需要的参数-FrequencyControlDTO
     */
    private SlidingWindowDTO buildSlidingWindowFrequencyControlDTO(String key, FrequencyControl frequencyControl) {
        SlidingWindowDTO frequencyControlDTO = new SlidingWindowDTO();
        frequencyControlDTO.setWindowSize(frequencyControl.windowSize());
        frequencyControlDTO.setPeriod(frequencyControl.period());
        frequencyControlDTO.setCount(frequencyControl.count());
        frequencyControlDTO.setUnit(frequencyControl.unit());
        frequencyControlDTO.setKey(key);
        return frequencyControlDTO;
    }

    /**
     * 将注解参数转换为编程式调用所需要的参数
     *
     * @param key              频率控制Key
     * @param frequencyControl 注解
     * @return 编程式调用所需要的参数-FrequencyControlDTO
     */
    private TokenBucketDTO buildTokenBucketDTO(String key, FrequencyControl frequencyControl) {
        TokenBucketDTO tokenBucketDTO = new TokenBucketDTO(frequencyControl.capacity(), frequencyControl.refillRate());
        tokenBucketDTO.setKey(key);
        return tokenBucketDTO;
    }

    /**
     * 将注解参数转换为编程式调用所需要的参数
     *
     * @param key              频率控制Key
     * @param frequencyControl 注解
     * @return 编程式调用所需要的参数-FrequencyControlDTO
     */
    private FixedWindowDTO buildFixedWindowDTO(String key, FrequencyControl frequencyControl) {
        FixedWindowDTO fixedWindowDTO = new FixedWindowDTO();
        fixedWindowDTO.setCount(frequencyControl.count());
        fixedWindowDTO.setTime(frequencyControl.time());
        fixedWindowDTO.setUnit(frequencyControl.unit());
        fixedWindowDTO.setKey(key);
        return fixedWindowDTO;
    }
}
