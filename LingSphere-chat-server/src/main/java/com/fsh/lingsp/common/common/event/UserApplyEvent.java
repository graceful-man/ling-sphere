package com.fsh.lingsp.common.common.event;

import com.fsh.lingsp.common.user.domain.entity.UserApply;
import lombok.Getter;
import org.springframework.context.ApplicationEvent;

/**
 * 作者：fsh
 * 日期：2024/03/09
 * <p>
 * 描述：用户申请事件
 */
@Getter
public class UserApplyEvent extends ApplicationEvent {

    private UserApply userApply;
    /**
     * @param source 事件源，哪个类注册的事件
     * @param userApply
     */
    public UserApplyEvent(Object source, UserApply userApply) {
        super(source);
        this.userApply=userApply;
    }
}
