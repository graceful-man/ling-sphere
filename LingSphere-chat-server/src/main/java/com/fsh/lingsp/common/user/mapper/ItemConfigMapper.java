package com.fsh.lingsp.common.user.mapper;

import com.fsh.lingsp.common.user.domain.entity.ItemConfig;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 功能物品配置表 Mapper 接口
 * </p>
 *
 * @author ${author}
 * @since 2024-02-27
 */
public interface ItemConfigMapper extends BaseMapper<ItemConfig> {

}
