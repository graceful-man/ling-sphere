package com.fsh.lingsp.common.common.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RequestInfo {
    /**
     * ip地址
     */
    private String ip;

    /**
     * UID，（实质上的数据库中用户的主键id）
     */
    private Long uid;
}
