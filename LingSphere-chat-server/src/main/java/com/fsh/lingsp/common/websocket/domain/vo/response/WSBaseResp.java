package com.fsh.lingsp.common.websocket.domain.vo.response;

import lombok.Data;

/**
 * 作者：fsh
 * 日期：2024/02/22
 * <p>
 * 描述：ws的基本返回信息体
 */
@Data
public class WSBaseResp<T> {


    /**
     * @see com.fsh.lingsp.common.websocket.domain.enums.WSRespTypeEnum
     */
    private Integer type;

    /**
     * 泛型，因为要返回不同的信息体
     */
    private T data;

}
