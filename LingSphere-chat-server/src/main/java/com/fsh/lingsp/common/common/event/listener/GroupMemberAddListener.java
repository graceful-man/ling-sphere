package com.fsh.lingsp.common.common.event.listener;

import com.fsh.lingsp.common.chat.domain.entity.GroupMember;
import com.fsh.lingsp.common.chat.domain.vo.request.ChatMessageReq;
import com.fsh.lingsp.common.chat.service.ChatService;
import com.fsh.lingsp.common.chat.service.adapter.MemberAdapter;
import com.fsh.lingsp.common.chat.service.adapter.RoomAdapter;
import com.fsh.lingsp.common.chat.service.cache.GroupMemberCache;
import com.fsh.lingsp.common.common.event.GroupMemberAddEvent;
import com.fsh.lingsp.common.user.dao.UserDao;
import com.fsh.lingsp.common.chat.domain.entity.RoomGroup;
import com.fsh.lingsp.common.user.domain.entity.User;
import com.fsh.lingsp.common.user.service.cache.UserInfoCache;
import com.fsh.lingsp.common.user.service.impl.PushService;
import com.fsh.lingsp.common.websocket.domain.vo.response.WSBaseResp;
import com.fsh.lingsp.common.websocket.domain.vo.response.WSMemberChange;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.transaction.event.TransactionalEventListener;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 添加群成员监听器。
 * 有两处地方使用了这个监听器。
 *
 * 1. 新增群组
 * 2. 邀请入群
 *
 * @author zhongzb create on 2022/08/26
 */
@Slf4j
@Component
public class GroupMemberAddListener {
    @Autowired
    private ChatService chatService;
    @Autowired
    private UserInfoCache userInfoCache;
    @Autowired
    private UserDao userDao;
    @Autowired
    private GroupMemberCache groupMemberCache;
    @Autowired
    private PushService pushService;


    /**
     * 推送加群消息：“谁邀请了xx,xx,xx,xx 加入群聊”
     *
     * @param event 事件
     */
    @Async
    @TransactionalEventListener(classes = GroupMemberAddEvent.class, fallbackExecution = true)
    public void sendAddMsg(GroupMemberAddEvent event) {
        List<GroupMember> memberList = event.getMemberList();// 被邀成员集合
        RoomGroup roomGroup = event.getRoomGroup();// 群组
        Long inviteUid = event.getInviteUid();// 邀请人uid

        User user = userInfoCache.get(inviteUid);
        // 被邀成员uid集合
        List<Long> uidList = memberList.stream().map(GroupMember::getUid).collect(Collectors.toList());
        ChatMessageReq chatMessageReq = RoomAdapter.buildGroupAddMessage(roomGroup, user, userInfoCache.getBatch(uidList));
        chatService.sendMsg(chatMessageReq, User.UID_SYSTEM);
    }

    /**
     * 有新成员加入群，要推送给群内所有人信息。
     *
     * @param event 事件
     */
    @Async
    @TransactionalEventListener(classes = GroupMemberAddEvent.class, fallbackExecution = true)
    public void sendChangePush(GroupMemberAddEvent event) {
        List<GroupMember> memberList = event.getMemberList();
        RoomGroup roomGroup = event.getRoomGroup();

        List<Long> memberUidList = groupMemberCache.getMemberUidList(roomGroup.getRoomId());
        List<Long> uidList = memberList.stream().map(GroupMember::getUid).collect(Collectors.toList());
        List<User> users = userDao.listByIds(uidList);
        users.forEach(user -> {
            WSBaseResp<WSMemberChange> ws = MemberAdapter.buildMemberAddWS(roomGroup.getRoomId(), user);
            pushService.sendPushMsg(ws, memberUidList);
        });
        //移除缓存
        groupMemberCache.evictMemberUidList(roomGroup.getRoomId());
    }

}
