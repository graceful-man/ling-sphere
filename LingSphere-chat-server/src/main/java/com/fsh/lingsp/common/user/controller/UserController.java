package com.fsh.lingsp.common.user.controller;


import com.fsh.lingsp.common.common.domain.vo.resp.ApiResult;
import com.fsh.lingsp.common.common.utils.AssertUtil;
import com.fsh.lingsp.common.common.utils.RequestHolder;
import com.fsh.lingsp.common.user.domain.dto.ItemInfoDTO;
import com.fsh.lingsp.common.user.domain.dto.SummeryInfoDTO;
import com.fsh.lingsp.common.user.domain.enums.IdempotentEnum;
import com.fsh.lingsp.common.user.domain.enums.RoleEnum;
import com.fsh.lingsp.common.user.domain.vo.req.user.*;
import com.fsh.lingsp.common.user.domain.vo.resp.user.BadgeResp;
import com.fsh.lingsp.common.user.domain.vo.resp.user.UserInfoResp;
import com.fsh.lingsp.common.user.service.RoleService;
import com.fsh.lingsp.common.user.service.UserBackpackService;
import com.fsh.lingsp.common.user.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 * 用户表 前端控制器
 * </p>
 * @since 2024-02-23
 */
@RestController
@RequestMapping("/capi/user")
@Api(tags = "用户管理相关接口")
public class UserController {

    @Autowired
    private UserService userService;
    @Autowired
    private RoleService roleService;

    /**
     *  private
     * 获取用户个人信息
     */
    @ApiOperation("获取用户个人信息")
    @GetMapping("/userInfo")
    public ApiResult<UserInfoResp> getUserInfo(){
        return ApiResult.success(userService.getUserInfo());
    }

    /**
     * 用户聚合信息-返回的代表需要刷新的
     */
    @PostMapping("/public/summary/userInfo/batch")
    @ApiOperation("用户聚合信息-返回的代表需要刷新的")
    public ApiResult<List<SummeryInfoDTO>> getSummeryUserInfo(@Valid @RequestBody SummeryInfoReq req) {
        return ApiResult.success(userService.getSummeryUserInfo(req));
    }

    /**
     * 徽章聚合信息-返回的代表需要刷新的
     */
    @PostMapping("/public/badges/batch")
    @ApiOperation("徽章聚合信息-返回的代表需要刷新的")
    public ApiResult<List<ItemInfoDTO>> getItemInfo(@Valid @RequestBody ItemInfoReq req) {
        return ApiResult.success(userService.getItemInfo(req));
    }


    /**
     * private
     * 修改用户名
     */
    @PutMapping("/name")
    @ApiOperation("修改用户名")
    public ApiResult<Void> modifyName(@Valid @RequestBody ModifyNameReq req){
        userService.modifyName(req);
        return ApiResult.success();
    }


    @GetMapping("/badges")
    @ApiOperation("可选徽章预览")
    public ApiResult<List<BadgeResp>> badges(){
        return ApiResult.success(userService.badges(RequestHolder.get().getUid()));
    }


    @PutMapping("/badges")
    @ApiOperation("佩戴徽章")
    public ApiResult<Void> badges(@Valid @RequestBody WearingBadgeReq req){
        userService.wearingBadge(RequestHolder.get().getUid(),req.getItemId());
        return ApiResult.success();
    }

    @PutMapping("/black")
    @ApiOperation("拉黑用户")
    public ApiResult<Void> black(@Valid @RequestBody BlackReq req){
        //直接先判断 该用户有没有拉黑别人的权限，即有没有超管身份
        boolean hasPower = roleService.hasPower(RequestHolder.get().getUid(), RoleEnum.ADMIN);
        AssertUtil.isTrue(hasPower,"您不是超管，没有拉黑权限");
        //有权限则拉黑目标
        userService.black(req);
        return ApiResult.success();
    }


    //-----------------------以下均为测试接口，非正式-------------------------------------------

    @Autowired
    private UserBackpackService userBackpackService;

    @ApiOperation("测试私有接口")
    @GetMapping("/public/hello2")
    public ApiResult<UserInfoResp> hello2(){
        System.out.println(RequestHolder.get());
        return null;
    }

    @ApiOperation("发放物品")
    @GetMapping("/public/")
    public ApiResult<Void> acquireItem(){
        userBackpackService.acquireItem(7L, 2L,IdempotentEnum.UID,"购买");
        return ApiResult.success();
    }
}

