package com.fsh.lingsp.common.common.domain.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@AllArgsConstructor
@Getter
public enum YseOrNoEnum {
    NO(0,"否"),
    YES(1,"是")
    ;

    private Integer status;
    private String desc;

    private static Map<Integer, YseOrNoEnum> cache;

    static {
        cache = Arrays.stream(YseOrNoEnum.values()).collect(Collectors.toMap(YseOrNoEnum::getStatus, Function.identity()));
    }

    public static YseOrNoEnum of(Integer type) {
        return cache.get(type);
    }

    public static Integer toStatus(Boolean bool) {
        return bool ? YES.getStatus() : NO.getStatus();
    }
}
