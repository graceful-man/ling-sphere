package com.fsh.lingsp.common.user.domain.entity;

import cn.hutool.core.util.StrUtil;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.util.Objects;
import java.util.Optional;

/**
 * <p>
 * 用户ip信息
 * </p>
 */
@Data
public class IpInfo implements Serializable {

    private static final long serialVersionUID = 1L;
    //注册时的ip
    private String createIp;
    //注册时的ip详情
    private IpDetail createIpDetail;
    //最新登录的ip
    private String updateIp;
    //最新登录的ip详情
    private IpDetail updateIpDetail;

    /**
     * 更新ip
     */
    public void refreshIp(String ip) {
        if(StrUtil.isBlank(ip)){
            return;
        }
        updateIp=ip;
        if (StrUtil.isBlank(createIp)){
            //这说明是新注册的
            createIp=ip;
        }
    }

    /**
     * 需要刷新的ip，这里判断更新ip就够，初始化的时候ip也是相同的，只需要设置的时候多设置进去就行。
     *
     * 1.新用户注册，createIp 和 updateIp 是一样的。此时 updateIpDetail 是空的，
     *   那么这个方法就会 判断 updateIp 和 updateIpDetail里的ip ，显然是false。
     *   那么把updateIp返回，解析归属地，最后填入 updateIpDetail里面。
     *   最终，updateIp 和 updateIpDetail里的ip 是一样的。
     *
     * 2.用户扫码，ip没变。那么判断 updateIp 和 updateIpDetail里的ip ，显然是true。不用重新解析。
     *
     * 3.用户扫码，ip变了。 由之前的逻辑，createIp是不会变得，但是updateIp已经改变了变成了新的newIp。
     *   判断 newIp 和 updateIpDetail里的ip ，显然是false。
     *   .....同理和1一样。
     */
    public String needRefreshIp() {
        boolean flag = Optional.ofNullable(updateIpDetail)
                .map(IpDetail::getIp)
                .filter(ip -> Objects.equals(ip, updateIp))
                .isPresent();

        return flag?null:updateIp;
    }

    /**
     * 更新ipDetail。
     */
    public void refreshIpDetail(IpDetail ipDetail) {
        if (Objects.equals(createIp,ipDetail.getIp())){
            createIpDetail=ipDetail;
        }
        // 此时的updateIp就是新的扫码ip
        if (Objects.equals(updateIp,ipDetail.getIp())){
            updateIpDetail=ipDetail;
        }
    }
}