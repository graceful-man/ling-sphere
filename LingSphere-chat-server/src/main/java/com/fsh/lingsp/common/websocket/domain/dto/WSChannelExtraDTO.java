package com.fsh.lingsp.common.websocket.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 作者：fsh
 * 日期：2024/02/24
 * <p>
 * 描述：记录和前端连接的一些映射信息
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class WSChannelExtraDTO {
    /**
     * 前端如果登录了，记录uid
     */
    private Long uid;
}
