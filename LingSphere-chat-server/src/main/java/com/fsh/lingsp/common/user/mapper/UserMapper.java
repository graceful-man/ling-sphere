package com.fsh.lingsp.common.user.mapper;

import com.fsh.lingsp.common.user.domain.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @author ${author}
 * @since 2024-02-23
 */
public interface UserMapper extends BaseMapper<User> {

}
