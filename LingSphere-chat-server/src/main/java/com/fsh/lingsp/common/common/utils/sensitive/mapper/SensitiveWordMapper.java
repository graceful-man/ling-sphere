package com.fsh.lingsp.common.common.utils.sensitive.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fsh.lingsp.common.common.utils.sensitive.domain.SensitiveWord;

/**
 * 敏感词Mapper
 *
 * @author zhaoyuhang
 * @since 2023-05-21
 */
public interface SensitiveWordMapper extends BaseMapper<SensitiveWord> {

}