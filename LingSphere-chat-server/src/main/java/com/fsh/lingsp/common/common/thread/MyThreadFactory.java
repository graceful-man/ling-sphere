package com.fsh.lingsp.common.common.thread;

import lombok.AllArgsConstructor;

import java.util.concurrent.ThreadFactory;

@AllArgsConstructor
public class MyThreadFactory implements ThreadFactory {
    // 定义一个私有的ThreadFactory类型的变量original，用于保存原始的线程工厂对象
    private ThreadFactory original;

    /**
     * 重写ThreadFactory接口的newThread方法，用于创建新的线程
     */
    @Override
    public Thread newThread(Runnable r) {
        // 使用原始线程工厂对象创建一个新的线程
        Thread thread = original.newThread(r);

        // 设置新线程的未捕获异常处理器为MyUncaughtExceptionHandler的实例
        // 当线程因为未捕获的异常而终止时，会调用这个处理器来处理异常
        thread.setUncaughtExceptionHandler(new MyUncaughtExceptionHandler());//异常捕获

        // 返回创建好的线程对象
        return thread;
    }
}