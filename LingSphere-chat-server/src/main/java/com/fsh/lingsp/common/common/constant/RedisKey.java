package com.fsh.lingsp.common.common.constant;

public class RedisKey {
    //所有key的前缀，以项目名标识
    public static final String BASE_KEY="lingsphere";


    /**
     * 关联用户token的 key
     */
    public static final String USER_TOKEN_STR="userToken:uid_%d";

    /**
     * 用户的信息更新时间
     */
    public static final String USER_MODIFY_STRING = "userModify:uid_%d";

    /**
     * 用户信息
     */
    public static final String USER_INFO_STRING = "userInfo:uid_%d";

    /**
     * 用户的信息汇总
     */
    public static final String USER_SUMMARY_STRING = "userSummary:uid_%d";

    /**
     * 房间详情
     */
    public static final String ROOM_INFO_STRING = "roomInfo:roomId_%d";

    /**
     * 群组详情
     */
    public static final String GROUP_INFO_STRING = "groupInfo:roomId_%d";

    /**
     * 热门房间列表
     */
    public static final String HOT_ROOM_ZET = "hotRoom";

    /**
     * 在线用户列表
     */
    public static final String ONLINE_UID_ZET = "online";

    /**
     * 离线用户列表
     */
    public static final String OFFLINE_UID_ZET = "offline";

    /**
     * 群组详情
     */
    public static final String GROUP_FRIEND_STRING = "groupFriend:roomId_%d";


    /**
     * 统一调用此方法获取key
     */
    public static String getKey(String key,Object... o){
        return BASE_KEY+String.format(key,o);
    }



}
