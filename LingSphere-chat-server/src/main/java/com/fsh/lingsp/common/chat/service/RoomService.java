package com.fsh.lingsp.common.chat.service;

import com.fsh.lingsp.common.chat.domain.entity.RoomFriend;
import com.fsh.lingsp.common.chat.domain.entity.RoomGroup;

import java.util.List;

/**
 * <p>
 * 房间表 服务类
 * </p>
 *
 * @author ${author}
 * @since 2024-03-08
 */
public interface RoomService {

    /**
     * 两个人创建一个聊天房间。一个聊天会话窗口
     */
    RoomFriend createFriendRoom(List<Long> asList);

    /**
     * 禁用房间
     */
    void disableFriendRoom(List<Long> asList);

    RoomFriend getFriendRoom(Long uid, Long friendUid);

    RoomGroup createGroupRoom(Long uid);
}
