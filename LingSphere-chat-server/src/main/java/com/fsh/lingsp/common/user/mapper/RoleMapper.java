package com.fsh.lingsp.common.user.mapper;

import com.fsh.lingsp.common.user.domain.entity.Role;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 角色表 Mapper 接口
 * </p>
 *
 * @author ${author}
 * @since 2024-03-06
 */
public interface RoleMapper extends BaseMapper<Role> {

}
