package com.fsh.lingsp.common.common.aspect;

import cn.hutool.core.util.StrUtil;
import com.fsh.lingsp.common.common.annotation.FrequencyControl;
import com.fsh.lingsp.common.common.constant.FrequencyControlConstant;
import com.fsh.lingsp.common.common.domain.dto.FrequencyControlDTO;
import com.fsh.lingsp.common.common.service.frequencycontrol.FrequencyControlUtil;
import com.fsh.lingsp.common.common.utils.RequestHolder;
import com.fsh.lingsphere.utils.SpElUtils;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.fsh.lingsp.common.common.service.frequencycontrol.FrequencyControlStrategyFactory.TOTAL_COUNT_WITH_IN_FIX_TIME_FREQUENCY_CONTROLLER;

/**
 * Description: 频控实现
 * Author: <a href="https://github.com/zongzibinbin">abin</a>
 * Date: 2023-04-20
 */
@Slf4j
@Aspect
@Component
public class FrequencyControlAspect {

    /**
     * @Around表示环绕通知，后面跟着的表达式用于匹配目标方法，当方法上有@FrequencyControl或@FrequencyControlContainer注解时触发
     *
     * @param joinPoint 加入点
     * @return {@link Object }
     */
    @Around("@annotation(com.fsh.lingsp.common.common.annotation.FrequencyControl)||@annotation(com.fsh.lingsp.common.common.annotation.FrequencyControlContainer)")
    public Object around(ProceedingJoinPoint joinPoint) throws Throwable {
        // 获取被拦截方法的Method对象
        Method method = ((MethodSignature) joinPoint.getSignature()).getMethod();
        // 获取方法上的所有@FrequencyControl注解
        FrequencyControl[] annotationsByType = method.getAnnotationsByType(FrequencyControl.class);
        // 用于存储键和注解映射关系的Map
        Map<String, FrequencyControl> keyMap = new HashMap<>();
        // 默认策略设置为固定窗口计数
        String strategy = FrequencyControlConstant.TOTAL_COUNT_WITH_IN_FIX_TIME;

        // 遍历所有@FrequencyControl注解
        for (int i = 0; i < annotationsByType.length; i++) {
            // 获取当前遍历到的频控注解
            FrequencyControl frequencyControl = annotationsByType[i];
            // 生成键的前缀，如果注解的prefixKey为空，则使用方法的通用字符串表示加上索引作为前缀
            String prefix = StrUtil.isBlank(frequencyControl.prefixKey()) ?
                    /* 默认方法限定名 + 注解排名（可能多个）*/method.toGenericString() + ":index:" + i :
                    frequencyControl.prefixKey();

            String key = "";
            // 根据注解中定义的target生成键
            switch (frequencyControl.target()) {
                case EL:
                    // 如果target是EL，则解析SpEL表达式生成键
                    key = SpElUtils.parseSpEl(method, joinPoint.getArgs(), frequencyControl.spEl());
                    break;
                case IP:
                    // 如果target是IP，则从请求中获取IP作为键
                    key = RequestHolder.get().getIp();
                    break;
                case UID:
                    // 如果target是UID，则从请求中获取UID作为键
                    key = RequestHolder.get().getUid().toString();
            }
            // 将生成的键和注解存入映射关系Map中
            keyMap.put(prefix + ":" + key, frequencyControl);
        }
        // 将注解的参数转换为编程式调用需要的参数
        List<FrequencyControlDTO> frequencyControlDTOS = keyMap.entrySet().stream().map(entrySet -> buildFrequencyControlDTO(entrySet.getKey(), entrySet.getValue())).collect(Collectors.toList());
        // 调用编程式注解.我们选择的是最简单的固定时间的方式，在指定时间统计次数，超过就限流。通过expire来实现指定时间，以及过期重置的效果。
        return FrequencyControlUtil.executeWithFrequencyControlList(TOTAL_COUNT_WITH_IN_FIX_TIME_FREQUENCY_CONTROLLER, frequencyControlDTOS, joinPoint::proceed);
    }

    /**
     * 将注解参数转换为编程式调用所需要的参数
     *
     * @param key              频率控制Key
     * @param frequencyControl 注解
     * @return 编程式调用所需要的参数-FrequencyControlDTO
     */
    private FrequencyControlDTO buildFrequencyControlDTO(String key, FrequencyControl frequencyControl) {
        FrequencyControlDTO frequencyControlDTO = new FrequencyControlDTO();
        frequencyControlDTO.setCount(frequencyControl.count());
        frequencyControlDTO.setTime(frequencyControl.time());
        frequencyControlDTO.setUnit(frequencyControl.unit());
        frequencyControlDTO.setKey(key);
        return frequencyControlDTO;
    }
}
