package com.fsh.lingsp.common.user.dao;

import com.fsh.lingsp.common.common.domain.enums.YseOrNoEnum;
import com.fsh.lingsp.common.user.domain.entity.UserBackpack;
import com.fsh.lingsp.common.user.mapper.UserBackpackMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 用户背包表 服务实现类
 * </p>
 *
 * @author ${author}
 * @since 2024-02-27
 */
@Service
public class UserBackpackDao extends ServiceImpl<UserBackpackMapper, UserBackpack>{

    public Integer getCountByUidAndItemId(Long uid, Long itemId) {
            return lambdaQuery()
                    .eq(UserBackpack::getUid,uid)
                    .eq(UserBackpack::getItemId,itemId)
                    //这里的 status 0  ，是未使用。
                    .eq(UserBackpack::getStatus, YseOrNoEnum.NO.getStatus())
                    .count();
    }

    /**
     * 得到第一张未使用的改名卡。 根据主键id升序
     */
    public UserBackpack getFirstItem(Long uid, Long itemId) {
        return lambdaQuery()
                .eq(UserBackpack::getUid,uid)
                .eq(UserBackpack::getItemId,itemId)
                .eq(UserBackpack::getStatus,YseOrNoEnum.NO.getStatus())
                .orderByAsc(UserBackpack::getId)
                .last("limit 1")
                .one();
    }

    /**
     * 使用第一张卡
     *  1.使用改名卡 ， 状态置为1 。 使用了乐观锁。
     */
    public Boolean useItem(UserBackpack firstReNameCard) {
        return lambdaUpdate()
                .eq(UserBackpack::getId,firstReNameCard.getId())
                .eq(UserBackpack::getStatus,firstReNameCard.getStatus())
                .set(UserBackpack::getStatus,YseOrNoEnum.YES.getStatus())
                .update();
    }

    /**
     * 查询用户拥有的徽章
     *
     *  Q1:status为什么用的是 No？ 因为数据库中这个物品没有被使用是0 ，使用是1。
     *  前端拥有为1，  但是没有被使用就是拥有
     */
    public List<UserBackpack> getByItemIds(Long uid, List<Long> idList) {
        return lambdaQuery()
                .eq(UserBackpack::getUid,uid)
                .eq(UserBackpack::getStatus,YseOrNoEnum.NO.getStatus())
                .in(UserBackpack::getItemId,idList)
                .list();
    }

    /**
     * 批量查询用户拥有的徽章
     */
    public List<UserBackpack> getByItemIds(List<Long> uids, List<Long> itemIds) {
        return lambdaQuery().in(UserBackpack::getUid, uids)
                .in(UserBackpack::getItemId, itemIds)
                .eq(UserBackpack::getStatus, YseOrNoEnum.NO.getStatus())
                .list();
    }



    /**
     * 先根据幂等号查看数据库中该用户的背包表有没有 这个物品
     */
    public UserBackpack getByIdempotent(String idempotent) {
        return lambdaQuery()
                .eq(UserBackpack::getIdempotent,idempotent)
                .one();
    }
}
