package com.fsh.lingsp.common.common.utils.discover;

import cn.hutool.core.util.StrUtil;
//import org.jetbrains.annotations.Nullable;
import org.jsoup.nodes.Document;
import org.springframework.lang.Nullable;

/**
 * 作者：fsh
 * 日期：2024/03/15
 * <p>
 * 描述：声明一个公共类CommonUrlDiscover，它继承自AbstractUrlDiscover
 */
public class CommonUrlDiscover extends AbstractUrlDiscover {

    /**
     * 重写父类中的getTitle方法，用于从Document对象中获取标题
     *
     * @param document 网页Dom对象
     * @return {@link String }
     */
    @Nullable
    @Override
    public String getTitle(Document document) {
        // 直接返回Document对象的title属性
        return document.title();
    }

    /**
     * 重写父类中的getDescription方法，用于从Document对象中获取描述
     *
     * @param document 网页Dom对象
     * @return {@link String }
     */
    @Nullable
    @Override
    public String getDescription(Document document) {
        // 从Document对象的head部分选取name为description的meta标签，并获取其content属性值
        String description = document.head().select("meta[name=description]").attr("content");
        // 从Document对象的head部分选取name为keywords的meta标签，并获取其content属性值
        String keywords = document.head().select("meta[name=keywords]").attr("content");

        // 如果description不为空，则使用description，否则使用keywords。 因为不同的网站 描述放在不同的标签。
        String content = StrUtil.isNotBlank(description) ? description : keywords;

        // 如果content不为空，则返回content中第一个"。"之前的部分（即一句话的描述）
        // 只保留一句话的描述
        return StrUtil.isNotBlank(content) ? content.substring(0, content.indexOf("。")) : content;
    }


    /**
     * 重写父类中的getImage方法，用于从给定的url和Document对象中获取图片链接
     *
     * @param url      网址
     * @param document 网页Dom对象
     * @return {@link String }
     */
    @Nullable
    @Override
    public String getImage(String url, Document document) {
        // 1.从Document对象中选取type为image/x-icon的link标签，并获取其href属性值，通常这是网站的图标链接
        String image = document.select("link[type=image/x-icon]").attr("href");

        // 2.如果image为空，则尝试选取rel属性以icon结尾的link标签，并获取其href属性值
        // 这通常是另一种指定网站图标的方式
        String href = StrUtil.isEmpty(image) ? document.select("link[rel$=icon]").attr("href") : image;

        // 3.如果url中已经包含了"favicon"，则直接返回url
        if (StrUtil.containsAny(url, "favicon")) {
            return url;
        }

        // 尝试连接href所指向的链接，如果连接成功或者href已经包含了http，则返回href
        // isConnect方法用于检查链接是否有效，这里假设它是一个方法，用于判断给定的链接是否可以访问
        if (isConnect(StrUtil.startWith(href, "http") ? href : "http:" + href)) {
            return href;
        }

        // 如果以上条件都不满足，则将href与url组合成一个完整的链接并返回
        return StrUtil.format("{}/{}", url, StrUtil.removePrefix(href, "/"));
    }
}