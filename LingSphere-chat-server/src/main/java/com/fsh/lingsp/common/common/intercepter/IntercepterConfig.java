package com.fsh.lingsp.common.common.intercepter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * 作者：fsh
 * 日期：2024/03/01
 * <p>
 * 描述：拦截器配置
 */
@Configuration
public class IntercepterConfig implements WebMvcConfigurer {
    @Autowired
    private TokenIntercepter tokenIntercepter;
    @Autowired
    private CollectorIntecepter collectorIntecepter;
    @Autowired
    private BlackIntercepter blackIntercepter;
    /**
     *   注册拦截器
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(tokenIntercepter)
                .addPathPatterns("/capi/**");

        registry.addInterceptor(collectorIntecepter)
                .addPathPatterns("/capi/**");

        registry.addInterceptor(blackIntercepter)
                .addPathPatterns("/capi/**");
    }
}
