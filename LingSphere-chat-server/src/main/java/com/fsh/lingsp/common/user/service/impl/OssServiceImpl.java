package com.fsh.lingsp.common.user.service.impl;

import com.abin.mallchat.oss.MinIOTemplate;
import com.abin.mallchat.oss.domain.OssReq;
import com.abin.mallchat.oss.domain.OssResp;
import com.fsh.lingsp.common.common.utils.AssertUtil;
import com.fsh.lingsp.common.user.domain.enums.OssSceneEnum;
import com.fsh.lingsp.common.user.domain.vo.req.oss.UploadUrlReq;
import com.fsh.lingsp.common.user.service.OssService;
import io.minio.messages.Bucket;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Description:
 * Author: <a href="https://github.com/zongzibinbin">abin</a>
 * Date: 2023-06-20
 */
@Service
public class OssServiceImpl implements OssService {

    @Autowired
    private MinIOTemplate minIOTemplate;

    @Override
    public OssResp getUploadUrl(Long uid, UploadUrlReq req) {
        OssSceneEnum sceneEnum = OssSceneEnum.of(req.getScene());
        AssertUtil.isNotEmpty(sceneEnum, "场景有误");
        OssReq ossReq = OssReq.builder()
                .fileName(req.getFileName())
                .filePath(sceneEnum.getPath())
                .uid(uid)
                .build();
        return minIOTemplate.getPreSignedObjectUrl(ossReq);
    }

    /**
     * 获取所有桶的名字
     */
    @Override
    public List<String> getAllBucket() {
        List<Bucket> buckets = minIOTemplate.listBuckets();
        List<String> names = buckets.stream().map(Bucket::name).collect(Collectors.toList());
        return names;
    }
}
