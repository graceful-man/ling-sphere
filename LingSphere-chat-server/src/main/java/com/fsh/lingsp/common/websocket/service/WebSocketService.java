package com.fsh.lingsp.common.websocket.service;

import com.fsh.lingsp.common.websocket.domain.vo.response.WSBaseResp;
import io.netty.channel.Channel;

/**
 * 作者：fsh
 * 日期：2024/02/24
 * <p>
 * 描述：Web 套接字服务。专门管理websocket的逻辑，包括推拉。
 */
public interface WebSocketService {
    /**
     * 处理所有ws连接的事件
     *
     * @param channel 通道
     */
    void connect(Channel channel);

    /**
     * 处理用户登录请求，需要返回一张带code的二维码
     *
     * @param channel 通道
     */
    void handleLoginReq(Channel channel);

    /**
     * 用户下线统一处理
     */
    void removed(Channel channel);

    /**
     * 登陆成功的逻辑，通过code找到给channel推送消息
     */
    void scanLoginSuccess(Integer code, Long id);

    /**
     * 等待授权，正在授权中
     */
    void waitAuthorize(Integer code);

    /**
     * 前提是：用户已经在网站 扫码登录注册授权过了，前端已经存了之前的token。
     * 登录认证：前端建立ws连接，传过来保存的token，拿到用户的信息
     */
    void authorize(Channel channel, String token);


    /**
     * 推送消息给全部在线人员
     */
    void sendMsgToAll(WSBaseResp<?> resp);

    /**
     * 推送给 uid 对应的 人（实质是 有一个 uid---channel）
     */
    void sendToUid(WSBaseResp<?> wsBaseMsg, Long uid);

    /**
     * 推动消息给所有在线的人
     *
     * @param wsBaseResp 发送的消息体
     * @param skipUid    需要跳过的人
     */
    void sendToAllOnline(WSBaseResp<?> wsBaseResp, Long skipUid);
}
