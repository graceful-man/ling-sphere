package com.fsh.lingsp.common.user.controller;

import com.fsh.lingsp.common.common.domain.vo.req.CursorPageBaseReq;
import com.fsh.lingsp.common.common.domain.vo.req.PageBaseReq;
import com.fsh.lingsp.common.common.domain.vo.resp.ApiResult;
import com.fsh.lingsp.common.common.domain.vo.resp.CursorPageBaseResp;
import com.fsh.lingsp.common.common.domain.vo.resp.PageBaseResp;
import com.fsh.lingsp.common.common.utils.RequestHolder;
import com.fsh.lingsp.common.user.domain.vo.req.friend.FriendApplyReq;
import com.fsh.lingsp.common.user.domain.vo.req.friend.FriendApproveReq;
import com.fsh.lingsp.common.user.domain.vo.req.friend.FriendDeleteReq;
import com.fsh.lingsp.common.user.domain.vo.resp.friend.FriendApplyResp;
import com.fsh.lingsp.common.user.domain.vo.resp.friend.FriendCheckResp;
import com.fsh.lingsp.common.user.domain.vo.req.friend.FriendCheckReq;
import com.fsh.lingsp.common.user.domain.vo.resp.friend.FriendResp;
import com.fsh.lingsp.common.user.domain.vo.resp.friend.FriendUnreadResp;
import com.fsh.lingsp.common.user.service.FriendService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * 作者：fsh
 * 日期：2024/03/08
 *
 * 好友相关接口。只有登录态才能访问的接口
 */
@RestController
@RequestMapping("/capi/user/friend")
@Api(tags = "好友相关接口")
@Slf4j
public class FriendController {

    @Autowired
    private FriendService friendService;


    /**
     * @param req 游标翻页请求参数
     *
     *  获取当前用户的好友列表。包装CursorPageBaseResp实体返回
     */
    @GetMapping("/page")
    @ApiOperation("联系人列表")
    public ApiResult<CursorPageBaseResp<FriendResp>> friendList(@Valid CursorPageBaseReq req) {
        Long uid = RequestHolder.get().getUid();
        return ApiResult.success(friendService.friendList(uid, req));
    }

    /**
     * 批量判断是否是自己好友
     */
    @GetMapping("/check")
    @ApiOperation("批量判断是否是自己好友")
    public ApiResult<FriendCheckResp> check(@Valid FriendCheckReq request) {
        Long uid = RequestHolder.get().getUid();
        return ApiResult.success(friendService.check(uid, request));
    }

    /**
     * 申请好友
     */
    @PostMapping("/apply")
    @ApiOperation("申请好友")
    public ApiResult<Void> apply(@Valid @RequestBody FriendApplyReq request) {
        Long uid = RequestHolder.get().getUid();
        friendService.apply(uid, request);
        return ApiResult.success();
    }

    /**
     * 普通翻页 获取好友申请列表
     */
    @GetMapping("/apply/page")
    @ApiOperation("好友申请列表")
    public ApiResult<PageBaseResp<FriendApplyResp>> page(@Valid PageBaseReq request) {
        Long uid = RequestHolder.get().getUid();
        return ApiResult.success(friendService.pageApplyFriend(uid, request));
    }

    /**
     * 返回申请列表的未读数
     */
    @GetMapping("/apply/unread")
    @ApiOperation("申请列表的未读数")
    public ApiResult<FriendUnreadResp> unread() {
        Long uid = RequestHolder.get().getUid();
        return ApiResult.success(friendService.unread(uid));
    }

    /**
     * 同意好友请求
     */
    @PutMapping("/apply")
    @ApiOperation("同意好友请求")
    public ApiResult<Void> applyApprove(@Valid @RequestBody FriendApproveReq request) {
        friendService.applyApprove(RequestHolder.get().getUid(), request);
        return ApiResult.success();
    }

    /**
     * 删除好友
     */
    @DeleteMapping()
    @ApiOperation("删除好友")
    public ApiResult<Void> delete(@Valid @RequestBody FriendDeleteReq request) {
        Long uid = RequestHolder.get().getUid();
        friendService.deleteFriend(uid, request.getTargetUid());
        return ApiResult.success();
    }

}

