package com.fsh.lingsp.common.chat.service;


import com.fsh.lingsp.common.chat.domain.vo.request.ChatMessageMemberReq;
import com.fsh.lingsp.common.chat.domain.vo.request.GroupAddReq;
import com.fsh.lingsp.common.chat.domain.vo.request.member.MemberAddReq;
import com.fsh.lingsp.common.chat.domain.vo.request.member.MemberDelReq;
import com.fsh.lingsp.common.chat.domain.vo.request.member.MemberReq;
import com.fsh.lingsp.common.chat.domain.vo.response.ChatMemberListResp;
import com.fsh.lingsp.common.chat.domain.vo.response.ChatRoomResp;
import com.fsh.lingsp.common.chat.domain.vo.response.MemberResp;
import com.fsh.lingsp.common.common.domain.vo.req.CursorPageBaseReq;
import com.fsh.lingsp.common.common.domain.vo.resp.CursorPageBaseResp;
import com.fsh.lingsp.common.websocket.domain.vo.response.ChatMemberResp;

import java.util.List;

/**
 * Description:
 * Author: <a href="https://github.com/zongzibinbin">abin</a>
 * Date: 2023-07-22
 */
public interface RoomAppService {
    /**
     * 获取会话列表--支持未登录态
     */
    CursorPageBaseResp<ChatRoomResp> getContactPage(CursorPageBaseReq request, Long uid);

    /**
     * 获取群组信息
     */
    MemberResp getGroupDetail(Long uid, long roomId);

    /**
     * 群成员列表
     *
     * @param request 请求
     * @return {@link CursorPageBaseResp }<{@link ChatMemberResp }>
     */
    CursorPageBaseResp<ChatMemberResp> getMemberPage(MemberReq request);

    /**
     * 房间内的所有群成员列表-@专用
     *
     * @param request 请求
     * @return {@link List }<{@link ChatMemberListResp }>
     */
    List<ChatMemberListResp> getMemberList(ChatMessageMemberReq request);

    /**
     * 移除成员
     *
     * @param uid     UID
     * @param request 请求
     */
    void delMember(Long uid, MemberDelReq request);

    void addMember(Long uid, MemberAddReq request);

    /**
     * 新增群组
     *
     * @param uid     UID
     * @param request 请求
     * @return {@link Long }
     */
    Long addGroup(Long uid, GroupAddReq request);

    ChatRoomResp getContactDetail(Long uid, Long roomId);

    ChatRoomResp getContactDetailByFriend(Long uid, Long friendUid);
}
