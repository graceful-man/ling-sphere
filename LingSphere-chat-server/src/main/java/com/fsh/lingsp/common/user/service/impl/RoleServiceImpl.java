package com.fsh.lingsp.common.user.service.impl;

import com.fsh.lingsp.common.user.domain.enums.RoleEnum;
import com.fsh.lingsp.common.user.service.RoleService;
import com.fsh.lingsp.common.user.service.cache.UserCache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;

/**
 * 作者：fsh
 * 日期：2024/03/06
 * <p>
 * 描述：角色服务实现
 */
@Service
public class RoleServiceImpl implements RoleService {

    @Autowired
    private UserCache userCache;

    /**
     * 用户是否拥有某个权限。 临时写法
     *
     * @param uid  用户表的id
     * @param roleEnum 用户传递的角色枚举
     */
    @Override
    public boolean hasPower(Long uid, RoleEnum roleEnum) {
        //获取该用户拥有的所有角色id
        Set<Long> roleSet = userCache.getRoleSet(uid);
        //首先判断该用户是不是超级管理员(拥有所有权限),其次判断用户传递的角色是否真的有。
        return isAdmin(roleSet) || roleSet.contains(roleEnum.getId());
    }

    //判断该用户所拥有的角色是否有 超级管理员
    private boolean isAdmin(Set<Long> roleSet){
        return roleSet.contains(RoleEnum.ADMIN.getId());
    }
}
