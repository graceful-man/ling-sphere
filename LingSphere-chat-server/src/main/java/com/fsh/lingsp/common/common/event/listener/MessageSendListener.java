package com.fsh.lingsp.common.common.event.listener;

import com.abin.mallchat.transaction.service.MQProducer;
import com.fsh.lingsp.common.chat.dao.MessageDao;
import com.fsh.lingsp.common.chat.domain.entity.Message;
import com.fsh.lingsp.common.chat.domain.entity.Room;
import com.fsh.lingsp.common.chat.service.ChatService;
import com.fsh.lingsp.common.chat.service.WeChatMsgOperationService;
import com.fsh.lingsp.common.chat.service.cache.RoomCache;
import com.fsh.lingsp.common.common.constant.MQConstant;
import com.fsh.lingsp.common.common.domain.dto.MsgSendMessageDTO;
import com.fsh.lingsp.common.common.domain.enums.HotFlagEnum;
import com.fsh.lingsp.common.common.event.MessageSendEvent;
import com.fsh.lingsp.common.websocket.service.WebSocketService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
import org.springframework.transaction.event.TransactionPhase;
import org.springframework.transaction.event.TransactionalEventListener;

import javax.validation.constraints.NotNull;
import java.util.Objects;

@Component
@Slf4j
public class MessageSendListener {
    @Autowired
    private MessageDao messageDao;
//    @Autowired
//    private IChatAIService openAIService;
    @Autowired
    private WeChatMsgOperationService weChatMsgOperationService;
    @Autowired
    private RoomCache roomCache;
    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;
    @Autowired
    private MQProducer mqProducer;

    /**
     * 把方法放在事务执行前，这样就可以和父事务在一起了
     */
    @TransactionalEventListener(phase = TransactionPhase.BEFORE_COMMIT, classes = MessageSendEvent.class, fallbackExecution = true)
    public void messageRoute(MessageSendEvent event) {
        //获取消息Id
        Long msgId = event.getMsgId();
        // 发送MQ消息（topic=chat_send_msg）,消息内容和KEY都是 发送的消息的id
        mqProducer.sendSecureMsg(MQConstant.SEND_MSG_TOPIC, new MsgSendMessageDTO(msgId), msgId);
    }



    @TransactionalEventListener(classes = MessageSendEvent.class, fallbackExecution = true)
    public void handlerMsg(@NotNull MessageSendEvent event) {
        Message message = messageDao.getById(event.getMsgId());
        Room room = roomCache.get(message.getRoomId());
//        if (isHotRoom(room)) {
//            openAIService.chat(message);
//        }
    }

    public boolean isHotRoom(Room room) {
        return Objects.equals(HotFlagEnum.YES.getType(), room.getHotFlag());
    }

    /**
     * 给用户微信推送艾特好友的消息通知
     * （这个没开启，微信不让推）
     */
    @TransactionalEventListener(classes = MessageSendEvent.class, fallbackExecution = true)
    public void publishChatToWechat(@NotNull MessageSendEvent event) {
        Message message = messageDao.getById(event.getMsgId());
        if (Objects.nonNull(message.getExtra().getAtUidList())) {
            weChatMsgOperationService.publishChatMsgToWeChatUser(message.getFromUid(), message.getExtra().getAtUidList(),
                    message.getContent());
        }
    }
}
