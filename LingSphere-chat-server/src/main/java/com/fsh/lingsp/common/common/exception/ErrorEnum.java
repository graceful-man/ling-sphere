package com.fsh.lingsp.common.common.exception;

/**
 * 作者：fsh
 * 日期：2024/03/01
 * <p>
 * 描述：错误枚举接口
 */
public interface ErrorEnum {

    Integer getErrorCode();

    String getErrorMsg();
}
