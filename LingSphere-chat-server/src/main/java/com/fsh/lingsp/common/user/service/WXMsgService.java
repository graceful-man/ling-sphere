package com.fsh.lingsp.common.user.service;

import me.chanjar.weixin.common.bean.WxOAuth2UserInfo;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.message.WxMpXmlMessage;
import me.chanjar.weixin.mp.bean.message.WxMpXmlOutMessage;

public interface WXMsgService {

    /**
     * 用户扫码处理（已关注和未关注）
     */
    WxMpXmlOutMessage scan(WxMpXmlMessage wxMpXmlMessage, WxMpService wxMpService);

    /**
     * 拿到用户信息后，授权，保存用户的信息到数据库
     */
    void authorize(WxOAuth2UserInfo userInfo);
}
