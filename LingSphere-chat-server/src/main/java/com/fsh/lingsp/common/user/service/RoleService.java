package com.fsh.lingsp.common.user.service;

import com.fsh.lingsp.common.user.domain.entity.Role;
import com.baomidou.mybatisplus.extension.service.IService;
import com.fsh.lingsp.common.user.domain.enums.RoleEnum;

/**
 * 作者：fsh
 * 日期：2024/03/06
 *
 * 角色表 服务类
 */
public interface RoleService  {

    /**
     * 是否拥有某个权限。 临时写法
     *
     * 该用户uid 是否拥有 roleEnum这个橘色
     */
    boolean hasPower(Long uid, RoleEnum roleEnum);
}
