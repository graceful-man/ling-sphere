package com.fsh.lingsp.common.common.exception;


import cn.hutool.http.ContentType;
import com.fsh.lingsp.common.common.domain.vo.resp.ApiResult;
import com.fsh.lingsp.common.common.utils.JsonUtils;
import com.google.common.base.Charsets;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 作者：fsh
 * 日期：2024/03/01
 * <p>
 * 描述：HTTP 错误枚举
 */
@AllArgsConstructor
@NoArgsConstructor
public enum HttpErrorEnum {
    ACCESS_DENIED(401,"登录失效请重新登录"),
    IN_BLACKLIST(401,"您在黑名单中！")
    ;

    private Integer code;
    private String desc;

    /**
     * 包装响应错误码 ，发送失败错误
     */
    public void sendHttpError(HttpServletResponse response) throws IOException {
        response.setStatus(code);
        response.setContentType(ContentType.JSON.toString(Charsets.UTF_8));
        response.getWriter().write(JsonUtils.toStr(ApiResult.fail(code,desc)));
    }
}
