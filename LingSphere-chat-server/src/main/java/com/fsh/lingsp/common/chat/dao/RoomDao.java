package com.fsh.lingsp.common.chat.dao;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fsh.lingsp.common.chat.mapper.RoomMapper;
import com.fsh.lingsp.common.chat.domain.entity.Room;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * <p>
 * 功能物品配置表 服务实现类
 * </p>
 *
 * @author ${author}
 * @since 2024-02-27
 */
@Service
public class RoomDao extends ServiceImpl<RoomMapper, Room> {

    /**
     * 更新这个房间的  最新消息id ，最新发消息时间
     *
     * @param roomId  房间 ID
     * @param msgId   消息ID
     * @param msgTime 消息时间
     */
    public void refreshActiveTime(Long roomId, Long msgId, Date msgTime) {
        lambdaUpdate()
                .eq(Room::getId, roomId)
                .set(Room::getLastMsgId, msgId)
                .set(Room::getActiveTime, msgTime)
                .update();
    }
}
