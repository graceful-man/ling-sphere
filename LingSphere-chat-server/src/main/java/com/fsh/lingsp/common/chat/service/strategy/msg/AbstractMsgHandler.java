package com.fsh.lingsp.common.chat.service.strategy.msg;

import cn.hutool.core.bean.BeanUtil;
import com.fsh.lingsp.common.chat.dao.MessageDao;
import com.fsh.lingsp.common.chat.domain.entity.Message;
import com.fsh.lingsp.common.chat.domain.enums.MessageTypeEnum;
import com.fsh.lingsp.common.chat.domain.vo.request.ChatMessageReq;
import com.fsh.lingsp.common.chat.service.adapter.MessageAdapter;
import com.fsh.lingsp.common.common.utils.AssertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.lang.reflect.ParameterizedType;

/**
 * 作者：fsh
 * 日期：2024/03/10
 * <p>
 * 描述：
 * Description: 消息处理器抽象类
 */
public abstract class AbstractMsgHandler<Req> {
    @Autowired
    private MessageDao messageDao;

    private Class<Req> bodyClass;

    @PostConstruct
    private void init() {
        ParameterizedType genericSuperclass = (ParameterizedType) this.getClass().getGenericSuperclass();
        this.bodyClass = (Class<Req>) genericSuperclass.getActualTypeArguments()[0];
        MsgHandlerFactory.register(getMsgTypeEnum().getType(), this);
    }

    /**
     * 消息类型
     * @see MessageTypeEnum
     */
    abstract MessageTypeEnum getMsgTypeEnum();

    protected void checkMsg(Req body, Long roomId, Long uid) {}

    protected abstract void saveMsg(Message message, Req body);

    @Transactional
    public Long checkAndSaveMsg(ChatMessageReq request, Long uid) {
        //将body统一转成各自对应的  dto
        Req body = this.toBean(request.getBody());
        //统一校验
        AssertUtil.allCheckValidateThrow(body);

        //子类扩展校验（子类实现就调用，没有实现的就是调用本类的空方法）
        checkMsg(body, request.getRoomId(), uid);
        Message message = MessageAdapter.buildMsgSave(request, uid);
        //统一保存
        messageDao.save(message);
        //子类扩展保存
        saveMsg(message, body);
        return message.getId();
    }

    private Req toBean(Object body) {
        if (bodyClass.isAssignableFrom(body.getClass())) {
            return (Req) body;
        }
        return BeanUtil.toBean(body, bodyClass);
    }

    /**
     * 展示消息
     */
    public abstract Object showMsg(Message msg);

    /**
     * 被回复时——展示的消息
     */
    public abstract Object showReplyMsg(Message msg);

    /**
     * 会话列表——展示的消息
     */
    public abstract String showContactMsg(Message msg);

}
