package com.fsh.lingsp.common.user.service;

public interface IpService{
    /**
     * 异步解析用户的 ip详情
     */
    void refreshDetailAsync(Long id);
}
