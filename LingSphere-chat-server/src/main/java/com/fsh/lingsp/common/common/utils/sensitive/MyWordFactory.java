package com.fsh.lingsp.common.common.utils.sensitive;

import com.fsh.lingsp.common.common.utils.sensitive.dao.SensitiveWordDao;
import com.fsh.lingsp.common.common.utils.sensitive.domain.SensitiveWord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class MyWordFactory implements IWordFactory {
    @Autowired
    private SensitiveWordDao sensitiveWordDao;

    /**
     * 先把敏感词全部查出来
     */
    @Override
    public List<String> getWordList() {
        return sensitiveWordDao.list()
                .stream()
                .map(SensitiveWord::getWord)
                .collect(Collectors.toList());
    }
}
