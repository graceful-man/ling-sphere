package com.fsh.lingsp.common.common.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum CommonErrorEnum implements ErrorEnum{
    PARAM_INVALID(-2,"参数校验失败"),
    SYSTEM_ERROR(-1, "系统出小差了，请稍后再试哦"),
    LOCK_LIMIT(-3,"请求太频繁了，请稍后重试..."),
    FREQUENCY_LIMIT(-4, "请求太频繁了，请稍后再试哦~~"),
    ;

    private Integer code;
    private String msg;

    @Override
    public Integer getErrorCode() {
        return code;
    }

    @Override
    public String getErrorMsg() {
        return msg;
    }
}
