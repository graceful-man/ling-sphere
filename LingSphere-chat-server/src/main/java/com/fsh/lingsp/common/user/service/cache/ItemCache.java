package com.fsh.lingsp.common.user.service.cache;

import com.fsh.lingsp.common.user.dao.ItemConfigDao;
import com.fsh.lingsp.common.user.domain.entity.ItemConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ItemCache {

    @Autowired
    private ItemConfigDao itemConfigDao;

    /**
     * 1.查询出所有的徽章；
     * 2.缓存
     */
    @Cacheable(cacheNames = "item",key = "'itemsByType:'+#type")
    public List<ItemConfig> getByType(Integer type){
        return itemConfigDao.getBadgeByType(type);
    }

    /**
     * 1.根据id查询出徽章；
     * 2.缓存
     */
    @Cacheable(cacheNames = "item", key = "'item:'+#itemId")
    public ItemConfig getById(Long itemId) {
        return itemConfigDao.getById(itemId);
    }
}
