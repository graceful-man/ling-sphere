package com.fsh.lingsp.common.user.domain.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import java.time.LocalDateTime;

import com.baomidou.mybatisplus.annotation.TableField;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

import com.baomidou.mybatisplus.extension.handlers.JacksonTypeHandler;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.*;

/**
 * <p>
 * 用户表
 * </p>
 *
 * @author ${author}
 * @since 2024-02-23
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName(value="user",autoResultMap = true)
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("用户实体")
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    public static Long UID_SYSTEM = 1L;//系统uid

    /**
     * 用户id
     */
    @ApiModelProperty("用户id")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 用户昵称
     */
    @TableField("name")
    @ApiModelProperty("用户昵称")
    private String name;

    /**
     * 用户头像
     */
    @TableField("avatar")
    @ApiModelProperty("用户头像")
    private String avatar;

    /**
     * 性别 1为男性，2为女性
     */
    @TableField("sex")
    @ApiModelProperty("性别 1为男性，2为女性")
    private Integer sex;

    /**
     * 微信openid用户标识
     */
    @TableField("open_id")
    @ApiModelProperty("微信openid用户标识")
    private String openId;

    /**
     * 在线状态 1在线 2离线
     */
    @TableField("active_status")
    @ApiModelProperty(" 在线状态 1在线 2离线")
    private Integer activeStatus;

    /**
     * 最后上下线时间
     */
    @TableField("last_opt_time")
    @ApiModelProperty("最后上下线时间")
    private Date lastOptTime;

    /**
     * ip信息
     */
    @TableField(value = "ip_info" , typeHandler = JacksonTypeHandler.class)
    @ApiModelProperty("ip信息")
    private IpInfo ipInfo;

    /**
     * 佩戴的徽章id
     */
    @TableField("item_id")
    @ApiModelProperty("佩戴的徽章id")
    private Long itemId;

    /**
     * 使用状态 0.正常 1拉黑
     */
    @TableField("status")
    @ApiModelProperty("使用状态 0.正常 1拉黑")
    private Integer status;

    /**
     * 创建时间
     */
    @TableField("create_time")
    @ApiModelProperty("创建时间")
    private Date createTime;

    /**
     * 修改时间
     */
    @TableField("update_time")
    @ApiModelProperty("修改时间")
    private Date updateTime;


    /**
     * 更新ip的方法
     */
    public void refreshIp(String ip) {
        if (Objects.isNull(this.ipInfo)){
            ipInfo=new IpInfo();
        }
        ipInfo.refreshIp(ip);
    }
}
