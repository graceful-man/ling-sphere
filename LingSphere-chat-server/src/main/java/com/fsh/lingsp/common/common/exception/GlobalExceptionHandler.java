package com.fsh.lingsp.common.common.exception;


import com.fsh.lingsp.common.common.domain.vo.resp.ApiResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * 作者：fsh
 * 日期：2024/03/01
 * <p>
 * 描述：全局异常捕获
 */
@RestControllerAdvice
@Slf4j
public class GlobalExceptionHandler{

    /**
     * 处理接口参数校验框架抛出的异常
     */
    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    public ApiResult<?> methodArgumentNotValidExceptionExceptionHandler(MethodArgumentNotValidException e) {
        StringBuilder errorMsg = new StringBuilder();
        e.getBindingResult().getFieldErrors()
                .forEach(x -> errorMsg.append(x.getField())
                        .append(x.getDefaultMessage())
                        .append(","));
        //去掉最后的 ，号，直接截取字符串，长度减1
        String message = errorMsg.toString();
        log.info("validation parameters error！The reason is:{},{}", message,e);
        return ApiResult.fail(CommonErrorEnum.PARAM_INVALID.getCode(), message.substring(0, message.length() - 1));
    }



    /**
     * 捕获 自定义的业务异常
     */
    @ExceptionHandler(value = BusinessException.class)
    public ApiResult<?> business(BusinessException e){
        log.info("business exception！The reason is：{},{}", e.getMessage(), e);
        return ApiResult.fail(e.getErrorCode(), e.getMessage());
    }



    /**
     * 抛异常 ，最后一道防线
     */
    @ExceptionHandler(value = Throwable.class)
    public ApiResult<?> AllError(Throwable e){
        log.error("system exception！The reason is：{}", e.getMessage(), e);
        return ApiResult.fail(CommonErrorEnum.SYSTEM_ERROR);
    }

}
