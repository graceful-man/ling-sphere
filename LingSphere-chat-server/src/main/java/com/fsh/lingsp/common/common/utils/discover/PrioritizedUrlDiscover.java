package com.fsh.lingsp.common.common.utils.discover;

import cn.hutool.core.util.StrUtil;
//import org.jetbrains.annotations.Nullable;
import org.jsoup.nodes.Document;
import org.springframework.lang.Nullable;

import java.util.ArrayList;
import java.util.List;

/**
 * 作者：fsh
 * 日期：2024/03/15
 * <p>
 * 描述：
 * Description: 具有优先级的title查询器。
 * 很像责任链模式，它会按顺序执行责任链，直到解析出url标题。
 *
 * 声明一个公共类PrioritizedUrlDiscover，它继承自AbstractUrlDiscover
 */
public class PrioritizedUrlDiscover extends AbstractUrlDiscover {

    // 定义一个私有的final的List来保存UrlDiscover对象，初始容量为2
    private final List<UrlDiscover> urlDiscovers = new ArrayList<>(2);

    // PrioritizedUrlDiscover的构造方法
    // 这两个的顺序有讲究，一般情况下大多是发送的平常普通网站，微信文章的网站一般很少发。
    public PrioritizedUrlDiscover() {
        // 在urlDiscovers列表中添加一个新的CommonUrlDiscover对象
        urlDiscovers.add(new CommonUrlDiscover());
        // 在urlDiscovers列表中添加一个新的WxUrlDiscover对象
        urlDiscovers.add(new WxUrlDiscover());
    }

    /**
     * 重写父类中的getTitle方法，尝试从Document对象中获取标题
     *
     * @param document 网页DOM对象
     * @return {@link String }
     */
    @Nullable
    @Override
    public String getTitle(Document document) {
        // 遍历urlDiscovers列表中的每一个UrlDiscover对象
        for (UrlDiscover urlDiscover : urlDiscovers) {
            // 调用当前UrlDiscover对象的getTitle方法，传入document对象，获取标题
            String urlTitle = urlDiscover.getTitle(document);
            // 如果获取的标题不为空（非空字符串且非空白）
            if (StrUtil.isNotBlank(urlTitle)) {
                // 返回找到的标题
                return urlTitle;
            }
        }
        // 如果所有UrlDiscover对象都没有找到标题，则返回null
        return null;
    }

    /**
     * 重写父类中的getDescription方法，尝试从Document对象中获取描述
     *
     * @param document 网页DOM对象
     * @return {@link String }
     */
    @Nullable
    @Override
    public String getDescription(Document document) {
        // 遍历urlDiscovers列表中的每一个UrlDiscover对象
        for (UrlDiscover urlDiscover : urlDiscovers) {
            // 调用当前UrlDiscover对象的getDescription方法，传入document对象，获取描述
            String urlDescription = urlDiscover.getDescription(document);
            // 如果获取的描述不为空（非空字符串且非空白）
            if (StrUtil.isNotBlank(urlDescription)) {
                // 返回找到的描述
                return urlDescription;
            }
        }
        // 如果所有UrlDiscover对象都没有找到描述，则返回null
        return null;
    }

    /**
     * 重写父类中的getImage方法，尝试从给定的url和Document对象中获取图片链接
     *
     * @param url      网址
     * @param document 网页DOM对象
     * @return {@link String }
     */
    @Nullable
    @Override
    public String getImage(String url, Document document) {
        // 遍历urlDiscovers列表中的每一个UrlDiscover对象
        for (UrlDiscover urlDiscover : urlDiscovers) {
            // 调用当前UrlDiscover对象的getImage方法，传入url和document对象，获取图片链接
            String urlImage = urlDiscover.getImage(url, document);
            // 如果获取的图片链接不为空（非空字符串且非空白）
            if (StrUtil.isNotBlank(urlImage)) {
                // 返回找到的图片链接
                return urlImage;
            }
        }
        // 如果所有UrlDiscover对象都没有找到图片链接，则返回null
        return null;
    }
}
