package com.fsh.lingsp.common.chat.service;

import com.fsh.lingsp.common.chat.domain.dto.MsgReadInfoDTO;
import com.fsh.lingsp.common.chat.domain.entity.Message;
import com.fsh.lingsp.common.chat.domain.vo.request.*;
import com.fsh.lingsp.common.chat.domain.vo.request.member.MemberReq;
import com.fsh.lingsp.common.chat.domain.vo.response.ChatMessageReadResp;
import com.fsh.lingsp.common.chat.domain.vo.response.ChatMessageResp;
import com.fsh.lingsp.common.chat.domain.vo.response.msg.ChatMemberStatisticResp;
import com.fsh.lingsp.common.common.domain.vo.resp.CursorPageBaseResp;
import com.fsh.lingsp.common.websocket.domain.vo.response.ChatMemberResp;

import java.util.Collection;
import java.util.List;

public interface ChatService {


    /**
     * 当前用户发送消息
     *
     * @param request 消息请求体
     * @param uid     当前用户的uid
     */
    Long sendMsg(ChatMessageReq request, Long uid);

    /**
     * 获取消息当前用户发送的消息
     * @param msgId 消息id
     * @param uid   当前用户的uid
     */
    ChatMessageResp getMsgResp(Long msgId, Long uid);

    /**
     * 根据消息获取消息前端展示的物料
     *
     * @param message
     * @param receiveUid 接受消息的uid，可null
     * @return
     */
    ChatMessageResp getMsgResp2(Message message, Long receiveUid);

    ChatMemberStatisticResp getMemberStatistic();

    /**
     * 获取消息列表
     */
    CursorPageBaseResp<ChatMessageResp> getMsgPage(ChatMessagePageReq request, Long uid);

    /**
     * 撤回消息
     * @param request 里面包括了 消息id 和 房间id
     */
    void recallMsg(Long uid, ChatMessageBaseReq request);

    /**
     * 消息标记。 （点赞点踩举报）
     *
     * @param uid     UID
     * @param request 消息标记请求体
     */
    void setMsgMark(Long uid, ChatMessageMarkReq request);

    CursorPageBaseResp<ChatMemberResp> getMemberPage(List<Long> memberUidList, MemberReq request);

    /**
     * 获取消息的已读未读总数
     *
     * @param uid     UID
     * @param request 请求 （本人的消息id集合）
     *                这个消息id集合 仅仅只是当前房间能看到的“我”发的消息。
     *                具体由 前端决定传递。
     */
    Collection<MsgReadInfoDTO> getMsgReadInfo(Long uid, ChatMessageReadInfoReq request);

    /**
     * 消息的已读未读列表。 已读和未读都会请求一次这个接口。
     *
     * @param uid     UID
     * @param request 请求
     * @return ChatMessageReadResp 已读或者未读的用户uid集合的游标翻页
     * 前端会拿着uid ，去浏览器缓存中找到用户的信息。
     */
    CursorPageBaseResp<ChatMessageReadResp>  getReadPage(Long uid, ChatMessageReadReq request);

    /**
     * 用户消息阅读上报
     *
     * @param uid     UID 用户uid
     * @param request 请求 （房间id）
     */
    void msgRead(Long uid, ChatMessageMemberReq request);
}
