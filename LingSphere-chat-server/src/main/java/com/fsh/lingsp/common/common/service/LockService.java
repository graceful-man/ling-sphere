package com.fsh.lingsp.common.common.service;

import com.fsh.lingsp.common.common.exception.BusinessException;
import com.fsh.lingsp.common.common.exception.CommonErrorEnum;
import lombok.SneakyThrows;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

/**
 * 作者：fsh
 * 日期：2024/03/02
 * <p>
 * 描述：分布式锁的工具类
 */
@Component
public class LockService {
    @Autowired
    private RedissonClient redissonClient;


    /**
     * 获取带有时间的锁。
     *
     * 使用了 lambda 表达式
     */
    @SneakyThrows
    public <T> T executeWithLock(String key, int waitTime, TimeUnit timeUnit, Supplier<T> supplier){
        RLock lock = redissonClient.getLock(key);
        boolean success = lock.tryLock(waitTime, timeUnit);
        if(!success){
            //加锁失败，抛出异常，由全局异常处理器捕获
            throw new BusinessException(CommonErrorEnum.LOCK_LIMIT);
        }
        try{
            return supplier.get();
        }finally {
            lock.unlock();
        }
    }

    /**
     * 获取锁，没有等待时间。获取不到锁就快速失败！
     *
     * 使用了 lambda 表达式
     */
    public <T> T executeWithLock(String key,  Supplier<T> supplier){
        return executeWithLock(key,-1,TimeUnit.MILLISECONDS,supplier);
    }

    /**
     * 获取锁，没有等待时间。获取不到锁就快速失败！
     *
     * 使用了 lambda 表达式
     */
    public <T> T executeWithLock(String key,  Runnable runnable){
        return executeWithLock(key, -1, TimeUnit.MILLISECONDS, new Supplier<T>() {
            @Override
            public T get() {
                runnable.run();
                return null;
            }
        });
    }


    public <T> T executeWithLockThrows(String key, int waitTime, TimeUnit unit, SupplierThrow<T> supplier) throws Throwable {
        RLock lock = redissonClient.getLock(key);
        boolean lockSuccess = lock.tryLock(waitTime, unit);
        if (!lockSuccess) {
            throw new BusinessException(CommonErrorEnum.LOCK_LIMIT);
        }
        try {
            return supplier.get();//执行锁内的代码逻辑
        } finally {
            if (lock.isLocked() && lock.isHeldByCurrentThread()) {
                lock.unlock();
            }
        }
    }


    @FunctionalInterface
    public interface SupplierThrow<T> {

        /**
         * Gets a result.
         *
         * @return a result
         */
        T get() throws Throwable;
    }
}
