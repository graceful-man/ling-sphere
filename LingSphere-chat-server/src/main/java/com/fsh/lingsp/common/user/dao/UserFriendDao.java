package com.fsh.lingsp.common.user.dao;

import com.fsh.lingsp.common.common.domain.enums.NormalOrNoEnum;
import com.fsh.lingsp.common.common.domain.vo.req.CursorPageBaseReq;
import com.fsh.lingsp.common.common.domain.vo.resp.CursorPageBaseResp;
import com.fsh.lingsp.common.common.utils.CursorUtils;
import com.fsh.lingsp.common.user.domain.entity.UserFriend;
import com.fsh.lingsp.common.user.mapper.UserFriendMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 用户联系人表 服务实现类
 * </p>
 *
 * @author ${author}
 * @since 2024-03-08
 */
@Service
public class UserFriendDao extends ServiceImpl<UserFriendMapper, UserFriend>  {

    /**
     * 游标翻页查询当前用户的好友列表
     */
    public CursorPageBaseResp<UserFriend> getFriendPage(Long uid, CursorPageBaseReq req) {
        return CursorUtils.getCursorPageByMysql(
                this,
                req,
                wrapper -> wrapper.eq(UserFriend::getUid, uid),
                UserFriend::getId);
    }

    /**
     * 批量判断是否是自己好友.
     *
     * 两个列表取交集就是自己的好友。
     * 无论谁in谁都一样，虽然我的好友列表in传过来的列表确实看着很怪。
     */
    public List<UserFriend> getByFriends(Long uid, List<Long> uidList) {
        return lambdaQuery()
                .eq(UserFriend::getUid,uid)
                .in(UserFriend::getFriendUid,uidList)
                .list();
    }

    /**
     * 查询双向好友关系的两条记录。并且只查询2条记录的主键id即可，因为用不到其他字段数据。
     *
     * 查询到的数据就是这样的
     * UserFriend(id=4, uid=null, friendUid=null, deleteStatus=null, createTime=null, updateTime=null)
     * UserFriend(id=3, uid=null, friendUid=null, deleteStatus=null, createTime=null, updateTime=null)
     */
    public List<UserFriend> getUserFriend(Long uid, Long friendUid) {
        return lambdaQuery()
                .eq(UserFriend::getUid, uid)
                .eq(UserFriend::getFriendUid, friendUid)
                .or()
                .eq(UserFriend::getFriendUid, uid)
                .eq(UserFriend::getUid, friendUid)
                .select(UserFriend::getId)
                .list();
    }

    /**
     * 确认两者是不是好友关系
     */
    public UserFriend getByFriend(Long uid, Long targetUid) {
        return lambdaQuery()
                .eq(UserFriend::getUid,uid)
                .eq(UserFriend::getFriendUid,targetUid)
                .eq(UserFriend::getDeleteStatus, NormalOrNoEnum.NORMAL.getStatus())
                .one();
    }
}
