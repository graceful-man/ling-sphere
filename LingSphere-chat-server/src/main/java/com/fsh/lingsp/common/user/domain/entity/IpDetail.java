package com.fsh.lingsp.common.user.domain.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 作者：fsh
 * 日期：2024/03/05
 * <p>
 * 用户ip信息详情
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor

//忽略未知属性
@JsonIgnoreProperties(ignoreUnknown = true)
public class IpDetail implements Serializable {

    private static final long serialVersionUID = 1L;
    //注册时的ip
    private String ip;
    //最新登录的ip
    private String isp;

    private String isp_id;
    private String city; //城市名
    private String city_id;//城市id
    private String country;//国家
    private String country_id;//国家id
    private String region;//区域
    private String region_id;//区域id
}