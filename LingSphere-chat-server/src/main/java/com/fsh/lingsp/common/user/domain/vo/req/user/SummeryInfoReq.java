package com.fsh.lingsp.common.user.domain.vo.req.user;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Size;
import java.util.List;


/**
 * 作者：fsh
 * 日期：2024/03/10
 * <p>
 * 描述：
 * Description: 批量查询用户汇总详情
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SummeryInfoReq {
    @ApiModelProperty(value = "用户信息入参")
    @Size(max = 50)
    private List<infoReq> reqList;

    @Data
    public static class infoReq {

        @ApiModelProperty(value = "uid")
        private Long uid;

        /**
         * 上次修改时间， 只要他小于数据库中  的lastModifyTime ，那就代表前端存的信息是旧信息，需要更新了。
         * 对应的是 user表中的  update_time 字段
         */
        @ApiModelProperty(value = "最近一次更新用户信息时间")
        private Long lastModifyTime;
    }
}
