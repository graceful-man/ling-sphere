package com.fsh.lingsp.common.user.service.impl;

import com.abin.mallchat.transaction.service.MQProducer;
import com.fsh.lingsp.common.common.constant.MQConstant;
import com.fsh.lingsp.common.common.domain.dto.PushMessageDTO;
import com.fsh.lingsp.common.websocket.domain.vo.response.WSBaseResp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 作者：fsh
 * 日期：2024/03/14
 * <p>
 * 描述：
 * Description: 推送消息的服务
 */
@Service
public class PushService {
    @Autowired
    private MQProducer mqProducer;

    /**
     * 向群内uidList中的人推送消息，。topic为 websocket_push
     * 这是 普通群聊或者单聊。
     */
    public void sendPushMsg(WSBaseResp<?> msg, List<Long> uidList) {
        mqProducer.sendMsg(MQConstant.PUSH_TOPIC, new PushMessageDTO(uidList, msg));
    }
    /**
     * 向所有在线的人推送消息，推送所有人。topic为 websocket_push
     * 这是 热点群聊。就是默认群聊，每一个用户都有的默认群聊
     */
    public void sendPushMsg(WSBaseResp<?> msg) {
        mqProducer.sendMsg(MQConstant.PUSH_TOPIC, new PushMessageDTO(msg));
    }

    public void sendPushMsg(WSBaseResp<?> msg, Long uid) {
        mqProducer.sendMsg(MQConstant.PUSH_TOPIC, new PushMessageDTO(uid, msg));
    }


}
