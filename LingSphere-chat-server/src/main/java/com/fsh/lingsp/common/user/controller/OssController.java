package com.fsh.lingsp.common.user.controller;

import com.abin.mallchat.oss.domain.OssResp;
import com.fsh.lingsp.common.common.domain.vo.resp.ApiResult;
import com.fsh.lingsp.common.common.utils.RequestHolder;
import com.fsh.lingsp.common.user.domain.vo.req.oss.UploadUrlReq;
import com.fsh.lingsp.common.user.service.OssService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

/**
 * Description: oss控制层
 * Author: <a href="https://github.com/zongzibinbin">abin</a>
 * Date: 2023-06-20
 */
@RestController
@RequestMapping("/capi/oss")
@Api(tags = "oss相关接口")
public class OssController {
    @Autowired
    private OssService ossService;

    @GetMapping("/upload/url")
    @ApiOperation("获取临时上传链接")
    public ApiResult<OssResp> getUploadUrl(@Valid UploadUrlReq req) {
        return ApiResult.success(ossService.getUploadUrl(RequestHolder.get().getUid(), req));
    }

    @GetMapping("/getAllBucket")
    @ApiOperation("获取minio中所有桶的名字")
    public List<String> getAllBucket(){
        return ossService.getAllBucket();
    }

}
