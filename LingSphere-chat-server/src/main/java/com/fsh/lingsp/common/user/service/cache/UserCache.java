package com.fsh.lingsp.common.user.service.cache;

import cn.hutool.core.collection.CollUtil;
import com.fsh.lingsp.common.common.constant.RedisKey;
import com.fsh.lingsp.common.common.utils.RedisUtils;
import com.fsh.lingsp.common.user.dao.BlackDao;
import com.fsh.lingsp.common.user.dao.UserDao;
import com.fsh.lingsp.common.user.dao.UserRoleDao;
import com.fsh.lingsp.common.user.domain.entity.Black;
import com.fsh.lingsp.common.user.domain.entity.User;
import com.fsh.lingsp.common.user.domain.entity.UserRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Component
public class UserCache {

    @Autowired
    private UserRoleDao userRoleDao;
    @Autowired
    private BlackDao blackDao;
    @Autowired
    private UserDao userDao;
    @Autowired
    private UserSummaryCache userSummaryCache;

    /**
     * 1.查询出该用户的信息；
     * 2.reids缓存
     */
    public User getUserInfo(Long uid) {
        return getUserInfoBatch(Collections.singleton(uid)).get(uid);
    }

    /**
     * 批量获取用户信息，盘路缓存模式。缓存到redis中
     *
     * @param uids 用户ID集合
     * @return 包含用户信息的Map，其中键为用户ID，值为对应的User对象
     */
    public Map<Long, User> getUserInfoBatch(Set<Long> uids) {
        // 批量组装Redis的key
        List<String> keys = uids.stream().map(a -> RedisKey.getKey(RedisKey.USER_INFO_STRING, a)).collect(Collectors.toList());
        // 使用Redis批量获取操作，从Redis中根据上一步生成的keys获取对应的User对象列表
        List<User> mget = RedisUtils.mget(keys, User.class);

        // 将获取到的User对象列表转换成Map，其中键为用户ID，值为对应的User对象
        // 使用stream的filter方法过滤掉null值，确保Map中不包含null值
        Map<Long, User> map = mget.stream().filter(Objects::nonNull).collect(Collectors.toMap(User::getId, Function.identity()));

        // 找出在原始uids集合中但不在map中的用户ID，这些是需要从数据库中加载的
        List<Long> needLoadUidList = uids.stream().filter(a -> !map.containsKey(a)).collect(Collectors.toList());

        // 如果存在需要加载的用户ID列表
        if (CollUtil.isNotEmpty(needLoadUidList)) {
            // 批量从数据库中加载用户信息
            List<User> needLoadUserList = userDao.listByIds(needLoadUidList);
            // 将从数据库中加载的用户信息转换成Map，其中键为Redis的key，值为对应的User对象
            Map<String, User> redisMap = needLoadUserList.stream()
                    .collect(Collectors.toMap(a -> RedisKey.getKey(RedisKey.USER_INFO_STRING, a.getId()), Function.identity()));
            // 将加载的用户信息批量设置到Redis中，并设置过期时间为5分钟
            RedisUtils.mset(redisMap, 5 * 60);
            // 将新加载的用户信息合并到原始的map中
            map.putAll(needLoadUserList.stream().collect(Collectors.toMap(User::getId, Function.identity())));
        }

        // 返回包含所有用户信息的Map
        return map;
    }

    /**
     * 用户的信息改变后，需要删除缓存
     */
    public void userInfoChange(Long uid) {
        // 删除redis中的缓存
        delUserInfo(uid);
        //删除UserSummaryCache，前端下次懒加载的时候可以获取到最新的数据
        userSummaryCache.delete(uid);
        //用户的信息改变后，更新最后一次 更新用户信息的时间
        refreshUserModifyTime(uid);
    }


    //更新  用户 最后一次修改信息  的时间。
    //根据uid构建key，set更新value。
    private void refreshUserModifyTime(Long uid) {
        String key = RedisKey.getKey(RedisKey.USER_MODIFY_STRING, uid);
        RedisUtils.set(key, new Date().getTime());
    }


    //删除用户信息在redis中的缓存。
    //根据uid构建key，删除这个key。
    private void delUserInfo(Long uid) {
        String key = RedisKey.getKey(RedisKey.USER_INFO_STRING, uid);
        RedisUtils.del(key);
    }


    /**
     * 1.查询出该用户拥有的所有角色id；
     * 2.缓存
     */
    @Cacheable(cacheNames = "user",key = "'roles:'+#uid")
    public Set<Long> getRoleSet(Long uid){
        // 查询出该用户拥有的所有角色
        List<UserRole> userRoleList=userRoleDao.getAllByUid(uid);
        //取出角色id
        Set<Long> set = userRoleList.stream()
                .map(userRole -> userRole.getRoleId())
                .collect(Collectors.toSet());
        return set;
    }

    /**
     * 1.查询出黑名单列表
     * 2.缓存。
     * 3.当黑名单列表更新时，就要清空缓存。↓
     */
    @Cacheable(cacheNames = "users",key = "'blackMap'")
    public Map<Integer, Set<String>> getBlackMap() {
        //从数据库查出获取黑名单列表
        List<Black> blackList = blackDao.list();
        //分组。 type为1是uid拉黑，为一组。  type为2是ip拉黑，为一组
        Map<Integer, List<Black>> collect = blackList.stream()
                .collect(Collectors.groupingBy(Black::getType));
        //构建 Map<Integer, Set<String>> 用来存储最终结果
        Map<Integer, Set<String>> blackMap=new HashMap<>();
        //遍历collect.entrySet(),也就是两个key1和2
        for (Map.Entry<Integer, List<Black>> entry : collect.entrySet()) {
            //获取每个key对应的List<Black>
            List<Black> list = entry.getValue();
            //将target属性取出来 转成set<String>
            Set<String> set = list.stream().map(Black::getTarget).collect(Collectors.toSet());
            //放进来
            blackMap.put(entry.getKey(),set);
        }
        return blackMap;
        //最终存的是
        // key=1，value=[uid1,uid2....]
        // key=2 , value=[ip1,ip2...]
    }

    /**
     * 3.当黑名单列表更新时，就要清空缓存.
     * 4.这个方法调用的时机。当有用户被扔进黑名单时。
     *      UserBlackListener中添加一个监听事件
     */
    @CacheEvict(cacheNames = "users",key = "'blackMap'")
    public Map<Integer, Set<String>> clearBlackMap(){
        return null;
    }


    /**
     * 从 redis 中查询：
     *  批量获取每个用户的 最近一次更新时间
     */
    public List<Long> getUserModifyTime(List<Long> uidList) {
        //批量获取用户的rediskey。  key为 lingsphereuserModify:uid_uid的具体值
        List<String> keys = uidList.stream()
                .map(uid -> RedisKey.getKey(RedisKey.USER_MODIFY_STRING, uid))
                .collect(Collectors.toList());
        //批量获取每个用户的 最近一次更新时间
        return RedisUtils.mget(keys,Long.class);
    }

    //用户下线
    public void offline(Long uid, Date optTime) {
        String onlineKey = RedisKey.getKey(RedisKey.ONLINE_UID_ZET);
        String offlineKey = RedisKey.getKey(RedisKey.OFFLINE_UID_ZET);
        //移除上线线表
        RedisUtils.zRemove(onlineKey, uid);
        //更新上线表
        RedisUtils.zAdd(offlineKey, uid, optTime.getTime());
    }

    public Long getOnlineNum() {
        String onlineKey = RedisKey.getKey(RedisKey.ONLINE_UID_ZET);
        return RedisUtils.zCard(onlineKey);
    }
}
