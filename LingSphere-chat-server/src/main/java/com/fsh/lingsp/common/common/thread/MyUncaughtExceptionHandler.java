package com.fsh.lingsp.common.common.thread;

import lombok.extern.slf4j.Slf4j;

/**
 * 作者：fsh
 * 日期：2024/02/26
 * <p>
 * 描述：捕获异常处理程序.
 *
 * 这个类的目的是为了提供一个自定义的异常处理机制。
 * 当线程中出现未捕获的异常时，它不会立即导致JVM崩溃，而是会记录错误日志，
 * 这样开发者可以更容易地找到和解决问题。要使用这个自定义的异常处理器，
 * 你需要使用Thread.setDefaultUncaughtExceptionHandler方法或者在创建线程时通过Thread构造函数的参数来设置它。
 */
@Slf4j
public class MyUncaughtExceptionHandler implements Thread.UncaughtExceptionHandler {

    /**
     * 当一个线程因为未捕获的异常而突然终止时，JVM会调用此方法。
     * 这个方法接受两个参数：一个是发生异常的线程对象t，另一个是异常对象e。
     */
    @Override
    public void uncaughtException(Thread t, Throwable e) {
        log.error("Exception in thread,{}",e);
    }
}
