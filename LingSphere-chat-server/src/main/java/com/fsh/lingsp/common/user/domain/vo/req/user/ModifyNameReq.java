package com.fsh.lingsp.common.user.domain.vo.req.user;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * 作者：fsh
 * 日期：2024/03/01
 * <p>
 * 描述：修改名称 req
 */
@Data
public class ModifyNameReq {
    @ApiModelProperty("新名字")
    @NotBlank
    @Length(max = 6,message = "用户名长度不能超过6位")
    private String name;
}
