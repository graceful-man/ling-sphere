package com.fsh.lingsp.common.user.service;

import com.fsh.lingsp.common.user.domain.dto.ItemInfoDTO;
import com.fsh.lingsp.common.user.domain.dto.SummeryInfoDTO;
import com.fsh.lingsp.common.user.domain.entity.User;
import com.fsh.lingsp.common.user.domain.vo.req.user.BlackReq;
import com.fsh.lingsp.common.user.domain.vo.req.user.ItemInfoReq;
import com.fsh.lingsp.common.user.domain.vo.req.user.SummeryInfoReq;
import com.fsh.lingsp.common.user.domain.vo.resp.user.BadgeResp;
import com.fsh.lingsp.common.user.domain.vo.req.user.ModifyNameReq;
import com.fsh.lingsp.common.user.domain.vo.resp.user.UserInfoResp;

import java.util.List;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author ${author}
 * @since 2024-02-23
 */
public interface UserService{

    /**
     * 用户注册
     */
    void register(User u);

    /**
     * 获取用户个人信息
     */
    UserInfoResp getUserInfo();


    /**
     * 修改用户名
     */
    void modifyName(ModifyNameReq req);

    /**
     * 可选徽章预览
     */
    List<BadgeResp> badges(Long uid);

    /**
     * 佩戴徽章
     */
    void wearingBadge(Long uid, Long itemId);

    /**
     * 拉黑目标
     */
    void black(BlackReq req);

    /**
     * 获取用户汇总信息
     */
    List<SummeryInfoDTO> getSummeryUserInfo(SummeryInfoReq req);

    /**
     * 获取徽章汇总信息
     */
    List<ItemInfoDTO> getItemInfo(ItemInfoReq req);

}
