package com.fsh.lingsp.common.chat.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.lang.Pair;
import com.fsh.lingsp.common.chat.dao.*;
import com.fsh.lingsp.common.chat.domain.dto.MsgReadInfoDTO;
import com.fsh.lingsp.common.chat.domain.entity.*;
import com.fsh.lingsp.common.chat.domain.enums.MessageMarkActTypeEnum;
import com.fsh.lingsp.common.chat.domain.enums.MessageTypeEnum;
import com.fsh.lingsp.common.chat.domain.vo.request.*;
import com.fsh.lingsp.common.chat.domain.vo.request.member.MemberReq;
import com.fsh.lingsp.common.chat.domain.vo.response.ChatMessageReadResp;
import com.fsh.lingsp.common.chat.domain.vo.response.ChatMessageResp;
import com.fsh.lingsp.common.chat.domain.vo.response.msg.ChatMemberStatisticResp;
import com.fsh.lingsp.common.chat.service.ContactService;
import com.fsh.lingsp.common.chat.service.adapter.MemberAdapter;
import com.fsh.lingsp.common.chat.service.adapter.RoomAdapter;
import com.fsh.lingsp.common.chat.service.helper.ChatMemberHelper;
import com.fsh.lingsp.common.chat.service.strategy.mark.AbstractMsgMarkStrategy;
import com.fsh.lingsp.common.chat.service.strategy.mark.MsgMarkFactory;
import com.fsh.lingsp.common.chat.service.strategy.msg.RecallMsgHandler;
import com.fsh.lingsp.common.common.domain.vo.req.CursorPageBaseReq;
import com.fsh.lingsp.common.common.domain.vo.resp.CursorPageBaseResp;
import com.fsh.lingsp.common.common.event.MessageSendEvent;
import com.fsh.lingsp.common.chat.service.ChatService;
import com.fsh.lingsp.common.chat.service.adapter.MessageAdapter;
import com.fsh.lingsp.common.chat.service.cache.RoomCache;
import com.fsh.lingsp.common.chat.service.cache.RoomGroupCache;
import com.fsh.lingsp.common.chat.service.strategy.msg.AbstractMsgHandler;
import com.fsh.lingsp.common.chat.service.strategy.msg.MsgHandlerFactory;
import com.fsh.lingsp.common.common.domain.enums.NormalOrNoEnum;
import com.fsh.lingsp.common.common.service.LockService;
import com.fsh.lingsp.common.common.utils.AssertUtil;
import com.fsh.lingsp.common.user.dao.UserDao;
import com.fsh.lingsp.common.user.domain.entity.User;
import com.fsh.lingsp.common.user.domain.enums.ChatActiveStatusEnum;
import com.fsh.lingsp.common.user.domain.enums.RoleEnum;
import com.fsh.lingsp.common.user.service.RoleService;
import com.fsh.lingsp.common.user.service.cache.UserCache;
import com.fsh.lingsp.common.websocket.domain.vo.response.ChatMemberResp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;

import javax.annotation.Nullable;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class ChatServiceImpl implements ChatService {

    @Autowired
    private MessageDao messageDao;
    @Autowired
    private MessageMarkDao messageMarkDao;
    @Autowired
    private RoomCache roomCache;
    @Autowired
    private UserCache userCache;
    @Autowired
    private RoomFriendDao roomFriendDao;
    @Autowired
    private ContactDao contactDao;
    @Autowired
    private RoleService roleService;
    @Autowired
    private RoomGroupCache roomGroupCache;
    @Autowired
    private GroupMemberDao groupMemberDao;
    @Autowired
    private RecallMsgHandler recallMsgHandler;
    @Autowired
    private LockService lockService;
    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;
    @Autowired
    private UserDao userDao;
    @Autowired
    private RoomGroupDao roomGroupDao;
    @Autowired
    private ContactService contactService;

    /**
     * 当前用户发送消息
     *
     * @param request 消息请求体
     * @param uid     当前用户的uid
     */
    @Override
    public Long sendMsg(ChatMessageReq request, Long uid) {
        check(request, uid);
        // 策略加工厂模式，得到具体发送消息的handler
        AbstractMsgHandler<?> msgHandler = MsgHandlerFactory.getStrategyNoNull(request.getMsgType());
        //将消息存入数据库
        Long msgId = msgHandler.checkAndSaveMsg(request, uid);

        //发布消息发送事件
        applicationEventPublisher.publishEvent(new MessageSendEvent(this, msgId));
        return msgId;
    }

    // 检查 房间相关信息。
    private void check(ChatMessageReq request, Long uid) {
        Room room = roomCache.get(request.getRoomId());
        //全员群跳过校验.因为所有人都默认在这个群
        if (room.isHotRoom()) {
            return;
        }
        if (room.isRoomFriend()) {
            RoomFriend roomFriend = roomFriendDao.getByRoomId(request.getRoomId());
            AssertUtil.equal(NormalOrNoEnum.NORMAL.getStatus(), roomFriend.getStatus(), "您已经被对方拉黑");
            AssertUtil.isTrue(uid.equals(roomFriend.getUid1()) || uid.equals(roomFriend.getUid2()), "您已经被对方拉黑");
        }
        if (room.isRoomGroup()) {
            RoomGroup roomGroup = roomGroupCache.get(request.getRoomId());
            GroupMember member = groupMemberDao.getMember(roomGroup.getId(), uid);
            AssertUtil.isNotEmpty(member, "您已经被移除该群");
        }
    }


    /**
     * 获取消息当前用户发送的消息
     * @param msgId 消息id
     * @param uid   当前用户的uid
     */
    @Override
    public ChatMessageResp getMsgResp(Long msgId, Long uid) {
        Message msg = messageDao.getById(msgId);
        return getMsgResp(msg, uid);
    }

    private ChatMessageResp getMsgResp(Message message, Long sendUid) {
        return CollUtil.getFirst(getMsgRespBatch(Collections.singletonList(message), sendUid));
    }

    private List<ChatMessageResp> getMsgRespBatch(List<Message> messages, Long sendUid) {
        if (CollectionUtil.isEmpty(messages)) {
            return new ArrayList<>();
        }
        //查询消息标志
        List<MessageMark> msgMark = messageMarkDao.getValidMarkByMsgIdBatch(messages.stream().map(Message::getId).collect(Collectors.toList()));
        return MessageAdapter.buildMsgResp(messages, msgMark, sendUid);
    }

    @Override
    public ChatMessageResp getMsgResp2(Message message, Long receiveUid) {
        return CollUtil.getFirst(getMsgRespBatch(Collections.singletonList(message), receiveUid));
    }

    @Override
    public ChatMemberStatisticResp getMemberStatistic() {
        System.out.println(Thread.currentThread().getName());
        Long onlineNum = userCache.getOnlineNum();
//        Long offlineNum = userCache.getOfflineNum();不展示总人数
        ChatMemberStatisticResp resp = new ChatMemberStatisticResp();
        resp.setOnlineNum(onlineNum);
//        resp.setTotalNum(onlineNum + offlineNum);
        return resp;
    }

    @Override
    public CursorPageBaseResp<ChatMessageResp> getMsgPage(ChatMessagePageReq request, Long receiveUid) {
        //拿到这个人 此刻 在这个房间看到的最新一条消息id。
        // 1.接着以这条消息id作为起点 ，游标翻页向上读取。游标翻页不会被新消息打扰(不会读到重复消息)，普通翻页会。
        // 2. 可以使用这条id，来限制被踢出的人能看见的最新一条消息。
        Long lastMsgId = getLastMsgId(request.getRoomId(), receiveUid);
        // 游标翻页，获取消息列表
        CursorPageBaseResp<Message> cursorPage = messageDao.getCursorPage(request.getRoomId(), request, lastMsgId);
        if (cursorPage.isEmpty()) {
            return CursorPageBaseResp.empty();
        }
        // 构造返回的消息体
        return CursorPageBaseResp.init(cursorPage, getMsgRespBatch(cursorPage.getList(), receiveUid));
    }

    private Long getLastMsgId(Long roomId, Long receiveUid) {
        Room room = roomCache.get(roomId);
        AssertUtil.isNotEmpty(room, "房间号有误");
        if (room.isHotRoom()) {//热点房间即默认房间，所有人都在，不会被踢出去。
            return null;
        }
        AssertUtil.isNotEmpty(receiveUid, "请先登录");
        //获取这个人在这个房间看到的最新一条消息id
        Contact contact = contactDao.get(receiveUid, roomId);
        return contact.getLastMsgId();
    }

    /**
     * 撤回消息
     *
     * @param uid  用户uid
     * @param request 里面包括了 消息id 和 房间id
     */
    @Override
    public void recallMsg(Long uid, ChatMessageBaseReq request) {
        // 查询出这条消息体
        Message message = messageDao.getById(request.getMsgId());
        //校验能不能执行撤回
        checkRecall(uid, message);
        //执行消息撤回
        recallMsgHandler.recall(uid, message);
    }

    // 校验uid  能不能执行撤回
    private void checkRecall(Long uid, Message message) {
        AssertUtil.isNotEmpty(message, "消息有误");
        AssertUtil.notEqual(message.getType(), MessageTypeEnum.RECALL.getType(), "消息无法撤回");
        //先判断这个uid  是不是管理员
        boolean hasPower = roleService.hasPower(uid, RoleEnum.CHAT_MANGER);
        if (hasPower) {
            return;
        }
        //没有管理员权限，再看看是不是消息的主人 。消息主人可以在2分钟内撤回。
        AssertUtil.equal(uid,message.getFromUid(),"抱歉,您没有权限");
        long between = DateUtil.between(message.getCreateTime(), new Date(), DateUnit.MINUTE);
        AssertUtil.isTrue(between < 2, "覆水难收，超过2分钟的消息不能撤回哦~~");
    }

    /**
     * 消息标记。 （点赞点踩举报）
     * todo 上 注解式分布式锁
     *
     * @param uid     UID
     * @param request 消息标记请求体
     */
    @Override
//    @RedissonLock(key = "#uid")
    public void setMsgMark(Long uid, ChatMessageMarkReq request) {
        lockService.executeWithLock(uid.toString(),()->{
            AbstractMsgMarkStrategy strategy = MsgMarkFactory.getStrategyNoNull(request.getMarkType());
            switch (MessageMarkActTypeEnum.of(request.getActType())) {
                case MARK:
                    strategy.mark(uid, request.getMsgId());
                    break;
                case UN_MARK:
                    strategy.unMark(uid, request.getMsgId());
                    break;
            }
        });
    }

    @Override
    public CursorPageBaseResp<ChatMemberResp> getMemberPage(List<Long> memberUidList, MemberReq request) {
        //解析游标 ， 在线_time  ，离线_time。
        // key为 在线or离线的enum  ，  value为 时间
        Pair<ChatActiveStatusEnum, String> pair = ChatMemberHelper.getCursorPair(request.getCursor());
        ChatActiveStatusEnum activeStatusEnum = pair.getKey();
        String timeCursor = pair.getValue();

        List<ChatMemberResp> resultList = new ArrayList<>();//最终列表
        Boolean isLast = Boolean.FALSE;
        if (activeStatusEnum == ChatActiveStatusEnum.ONLINE) {//在线列表
            CursorPageBaseResp<User> cursorPage = userDao.getCursorPage(memberUidList, new CursorPageBaseReq(request.getPageSize(), timeCursor), ChatActiveStatusEnum.ONLINE);
            resultList.addAll(MemberAdapter.buildMember(cursorPage.getList()));//添加在线列表
            if (cursorPage.getIsLast()) {//如果是最后一页,从离线列表再补点数据
                activeStatusEnum = ChatActiveStatusEnum.OFFLINE;
                Integer leftSize = request.getPageSize() - cursorPage.getList().size();
                cursorPage = userDao.getCursorPage(memberUidList, new CursorPageBaseReq(leftSize, null), ChatActiveStatusEnum.OFFLINE);
                resultList.addAll(MemberAdapter.buildMember(cursorPage.getList()));//添加离线线列表
            }
            timeCursor = cursorPage.getCursor();
            isLast = cursorPage.getIsLast();
        } else if (activeStatusEnum == ChatActiveStatusEnum.OFFLINE) {//离线列表
            CursorPageBaseResp<User> cursorPage = userDao.getCursorPage(memberUidList, new CursorPageBaseReq(request.getPageSize(), timeCursor), ChatActiveStatusEnum.OFFLINE);
            resultList.addAll(MemberAdapter.buildMember(cursorPage.getList()));//添加离线线列表
            timeCursor = cursorPage.getCursor();
            isLast = cursorPage.getIsLast();
        }
        // 获取群成员角色ID
        List<Long> uidList = resultList.stream().map(ChatMemberResp::getUid).collect(Collectors.toList());
        RoomGroup roomGroup = roomGroupDao.getByRoomId(request.getRoomId());
        Map<Long, Integer> uidMapRole = groupMemberDao.getMemberMapRole(roomGroup.getId(), uidList);
        resultList.forEach(member -> member.setRoleId(uidMapRole.get(member.getUid())));
        //组装结果
        return new CursorPageBaseResp<>(ChatMemberHelper.generateCursor(activeStatusEnum, timeCursor), isLast, resultList);
    }

    /**
     * 获取消息的已读未读总数
     *
     * @param uid     UID
     * @param request 请求 （本人的消息id集合）
     *                这个消息id集合 仅仅只是当前房间能看到的“我”发的消息。
     *                具体由 前端决定传递。
     */
    @Override
    public Collection<MsgReadInfoDTO> getMsgReadInfo(Long uid, ChatMessageReadInfoReq request) {
        // 获取这些消息
        List<Message> messages = messageDao.listByIds(request.getMsgIds());
        messages.forEach(message -> {
            AssertUtil.equal(uid, message.getFromUid(), "只能查询自己发送的消息");
        });
        return contactService.getMsgReadInfo(messages).values();
    }

    /**
     * 消息的已读未读列表。 已读和未读都会请求一次这个接口。思想和已读未读总数是一样的
     *
     * @param uid     UID
     * @param request 请求 （包含：消息id 和查询类型）
     * @return ChatMessageReadResp 已读或者未读的用户uid集合的游标翻页
     * 前端会拿着uid ，去浏览器缓存中找到用户的信息。
     */
    @Override
    public CursorPageBaseResp<ChatMessageReadResp> getReadPage(@Nullable Long uid, ChatMessageReadReq request) {
        Message message = messageDao.getById(request.getMsgId());
        AssertUtil.isNotEmpty(message, "消息id有误");
        AssertUtil.equal(uid, message.getFromUid(), "只能查看自己的消息");
        CursorPageBaseResp<Contact> page;
        if (request.getSearchType() == 1) {//已读
            page = contactDao.getReadPage(message, request);
        } else {
            page = contactDao.getUnReadPage(message, request);
        }
        if (CollectionUtil.isEmpty(page.getList())) {
            return CursorPageBaseResp.empty();
        }
        return CursorPageBaseResp.init(page, RoomAdapter.buildReadResp(page.getList()));
    }

    /**
     * 用户消息阅读上报（其实就是更新该uid用户在这个房间中的最新read_time时间）
     *
     * @param uid     UID 用户uid
     * @param request 请求 （房间id）
     */
    @Override
//    @RedissonLock(key = "#uid")  todo 注解式分布式锁
    public void msgRead(Long uid, ChatMessageMemberReq request) {
        lockService.executeWithLock(uid.toString(),()->{
            // 根据 uid，roomId 查询出，该uid在该房间 最新的contact记录
            Contact contact = contactDao.get(uid, request.getRoomId());
            if (Objects.nonNull(contact)) {//有记录
                Contact update = new Contact();
                update.setId(contact.getId());
                // 更新该uid在这个房间最新的阅读时间
                update.setReadTime(new Date());
                contactDao.updateById(update);
            } else {//没有记录，意味着第一次进入这个房间，那就新插入一条记录
                Contact insert = new Contact();
                insert.setUid(uid);
                insert.setRoomId(request.getRoomId());
                insert.setReadTime(new Date());
                contactDao.save(insert);
            }
        });
    }
}
