package com.fsh.lingsp.common.chat.service.strategy.msg;

import com.fsh.lingsp.common.common.exception.CommonErrorEnum;
import com.fsh.lingsp.common.common.utils.AssertUtil;

import java.util.HashMap;
import java.util.Map;

/**
 * 作者：fsh
 * 日期：2024/03/10
 * <p>
 * 描述：
 * Description: 消息处理器的工厂
 */
public class MsgHandlerFactory {

    /**
     * map 里面存的就是 每个消息处理器的具体实现。
     * key为 消息类型enum的type ，value是 对应消息处理器的具体实现。
     */
    private static final Map<Integer, AbstractMsgHandler> STRATEGY_MAP = new HashMap<>();

    public static void register(Integer code, AbstractMsgHandler strategy) {
        STRATEGY_MAP.put(code, strategy);
    }

    /**
     * 根据消息类型enum的type ，获取 对应消息处理器的具体实现。
     */
    public static AbstractMsgHandler getStrategyNoNull(Integer code) {
        AbstractMsgHandler strategy = STRATEGY_MAP.get(code);
        AssertUtil.isNotEmpty(strategy, CommonErrorEnum.PARAM_INVALID);
        return strategy;
    }
}
