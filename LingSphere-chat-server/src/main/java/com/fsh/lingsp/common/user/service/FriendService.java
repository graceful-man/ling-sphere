package com.fsh.lingsp.common.user.service;


import com.fsh.lingsp.common.common.domain.vo.req.CursorPageBaseReq;
import com.fsh.lingsp.common.common.domain.vo.req.PageBaseReq;
import com.fsh.lingsp.common.common.domain.vo.resp.CursorPageBaseResp;
import com.fsh.lingsp.common.common.domain.vo.resp.PageBaseResp;
import com.fsh.lingsp.common.user.domain.vo.req.friend.FriendApplyReq;
import com.fsh.lingsp.common.user.domain.vo.req.friend.FriendApproveReq;
import com.fsh.lingsp.common.user.domain.vo.req.friend.FriendCheckReq;
import com.fsh.lingsp.common.user.domain.vo.resp.friend.FriendApplyResp;
import com.fsh.lingsp.common.user.domain.vo.resp.friend.FriendCheckResp;
import com.fsh.lingsp.common.user.domain.vo.resp.friend.FriendResp;
import com.fsh.lingsp.common.user.domain.vo.resp.friend.FriendUnreadResp;

/**
 * 作者：fsh
 * 日期：2024/03/08
 * <p>
 * 描述：好友服务
 */
public interface FriendService {


    /**
     * 获取当前用户的好友列表
     */
    CursorPageBaseResp<FriendResp> friendList(Long uid, CursorPageBaseReq req);

    /**
     * 批量判断是否是自己好友
     */
    FriendCheckResp check(Long uid, FriendCheckReq request);

    /**
     * 普通翻页 获取好友申请列表
     */
    PageBaseResp<FriendApplyResp> pageApplyFriend(Long uid, PageBaseReq request);

    /**
     * 返回申请列表的未读数
     */
    FriendUnreadResp unread(Long uid);

    /**
     * 同意好友请求
     */
    void applyApprove(Long uid, FriendApproveReq request);

    /**
     * 删除好友
     */
    void deleteFriend(Long uid, Long targetUid);

    /**
     * 申请好友
     */
    void apply(Long uid, FriendApplyReq request);
}
