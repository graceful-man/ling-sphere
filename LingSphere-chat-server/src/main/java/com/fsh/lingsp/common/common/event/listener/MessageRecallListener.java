package com.fsh.lingsp.common.common.event.listener;

import com.fsh.lingsp.common.chat.domain.dto.ChatMsgRecallDTO;
import com.fsh.lingsp.common.chat.service.ChatService;
import com.fsh.lingsp.common.chat.service.cache.MsgCache;
import com.fsh.lingsp.common.common.event.MessageRecallEvent;
import com.fsh.lingsp.common.user.service.impl.PushService;
import com.fsh.lingsp.common.websocket.adapter.WSAdapter;
import com.fsh.lingsp.common.websocket.service.WebSocketService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.transaction.event.TransactionalEventListener;

/**
 * 消息撤回监听器
 *
 * @author zhongzb create on 2022/08/26
 */
@Slf4j
@Component
public class MessageRecallListener {
    @Autowired
    private WebSocketService webSocketService;
    @Autowired
    private ChatService chatService;
    @Autowired
    private MsgCache msgCache;
    @Autowired
    private PushService pushService;

    /**
     * 删除这条消息的缓存
     */
    @Async
    @TransactionalEventListener(classes = MessageRecallEvent.class, fallbackExecution = true)
    public void evictMsg(MessageRecallEvent event) {
        ChatMsgRecallDTO recallDTO = event.getRecallDTO();
        msgCache.evictMsg(recallDTO.getMsgId());
    }

    /**
     *
     */
    @Async
    @TransactionalEventListener(classes = MessageRecallEvent.class, fallbackExecution = true)
    public void sendToAll(MessageRecallEvent event) {
        // 推送给群成员  一条撤回消息。
        pushService.sendPushMsg(WSAdapter.buildMsgRecall(event.getRecallDTO()));
    }

}
