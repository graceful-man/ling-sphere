package com.fsh.lingsp.common.user.mapper;

import com.fsh.lingsp.common.user.domain.entity.Black;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 黑名单 Mapper 接口
 * </p>
 *
 * @author ${author}
 * @since 2024-03-06
 */
public interface BlackMapper extends BaseMapper<Black> {

}
