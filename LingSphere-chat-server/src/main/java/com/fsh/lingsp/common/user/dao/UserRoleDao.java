package com.fsh.lingsp.common.user.dao;

import com.fsh.lingsp.common.user.domain.entity.UserRole;
import com.fsh.lingsp.common.user.mapper.UserRoleMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 用户角色关系表 服务实现类
 * </p>
 *
 * @author ${author}
 * @since 2024-03-06
 */
@Service
public class UserRoleDao extends ServiceImpl<UserRoleMapper, UserRole> {

    /**
     * 查询出该用户拥有的所有角色
     */
    public List<UserRole> getAllByUid(Long uid) {
        return lambdaQuery()
                .eq(UserRole::getUid,uid)
                .list();
    }
}
