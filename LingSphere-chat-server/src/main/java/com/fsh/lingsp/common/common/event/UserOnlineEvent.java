package com.fsh.lingsp.common.common.event;

import com.fsh.lingsp.common.user.domain.entity.User;
import lombok.Getter;
import org.springframework.context.ApplicationEvent;

/**
 * 作者：fsh
 * 日期：2024/03/05
 * 描述：用户上线事件
 */
@Getter
public class UserOnlineEvent extends ApplicationEvent {

    private User user;
    /**
     * @param source 事件源，哪个类注册的事件
     */
    public UserOnlineEvent(Object source, User user) {
        super(source);
        this.user=user;
    }
}
