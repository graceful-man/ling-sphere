package com.fsh.lingsp.common.chat.service.impl;

import com.fsh.lingsp.common.chat.dao.GroupMemberDao;
import com.fsh.lingsp.common.chat.dao.RoomGroupDao;
import com.fsh.lingsp.common.chat.domain.entity.GroupMember;
import com.fsh.lingsp.common.chat.domain.entity.RoomGroup;
import com.fsh.lingsp.common.chat.domain.enums.GroupRoleEnum;
import com.fsh.lingsp.common.chat.service.RoomService;
import com.fsh.lingsp.common.common.domain.enums.NormalOrNoEnum;
import com.fsh.lingsp.common.common.utils.AssertUtil;
import com.fsh.lingsp.common.chat.dao.RoomDao;
import com.fsh.lingsp.common.chat.dao.RoomFriendDao;
import com.fsh.lingsp.common.chat.domain.entity.Room;
import com.fsh.lingsp.common.chat.domain.entity.RoomFriend;
import com.fsh.lingsp.common.user.domain.entity.User;
import com.fsh.lingsp.common.user.domain.enums.RoomTypeEnum;
import com.fsh.lingsp.common.user.service.adapter.ChatAdapter;
import com.fsh.lingsp.common.user.service.cache.UserInfoCache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

@Service
public class RoomServiceImpl implements RoomService {

    @Autowired
    private RoomFriendDao roomFriendDao;
    @Autowired
    private RoomDao roomDao;
    @Autowired
    private UserInfoCache userInfoCache;
    @Autowired
    private GroupMemberDao groupMemberDao;
    @Autowired
    private RoomGroupDao roomGroupDao;

    /**
     * 两个人创建一个聊天房间，一个聊天会话窗口
     */
    @Override
    public RoomFriend createFriendRoom(List<Long> uidList) {
        //先判断参数
        AssertUtil.isNotEmpty(uidList,"房间创建失败，好友数量不对");
        AssertUtil.isTrue(uidList.size()==2,"房间创建失败，好友数量不对");

        //构建房间的key。房间key由两个uid拼接，先做升序排序uid1_uid2
        String roomKey= ChatAdapter.generateRoomKey(uidList);

        //先查询看有没有这个房间
        RoomFriend roomFriend=roomFriendDao.getByRoomKey(roomKey);
        //不存在
        if (Objects.isNull(roomFriend)){
            //新建room
            Room room=createRoom(RoomTypeEnum.FRIEND);
            //新建roomFriend
            roomFriend=createFriendRoom(room.getId(),uidList);
        }else{
            //存在这个房间，那就恢复。适用于恢复好友的场景
            restoreRoomIfNeed(roomFriend);
        }
        return roomFriend;
    }

    // 恢复房间(聊天会话的窗口)。适用于恢复好友的场景
    private void restoreRoomIfNeed(RoomFriend roomFriend) {
        if (Objects.equals(roomFriend.getStatus(), NormalOrNoEnum.NOT_NORMAL.getStatus())){
            roomFriendDao.restoreRoom(roomFriend.getId());
        }
    }

    //新建roomFriend。  这里存的是 两个人的uid，roomKey，room的id ，等关联信息
    private RoomFriend createFriendRoom(Long roomId, List<Long> uidList) {
        RoomFriend insert=ChatAdapter.buildFriendRoom(roomId,uidList);
        roomFriendDao.save(insert);
        return insert;
    }

    //新建房间 room
    private Room createRoom(RoomTypeEnum roomTypeEnum) {
        Room insert=ChatAdapter.buildRoom(roomTypeEnum);
        roomDao.save(insert);
        return insert;
    }

    /**
     * 禁用房间
     *
     */
    @Override
    public void disableFriendRoom(List<Long> uidList) {
        //先判断参数
        AssertUtil.isNotEmpty(uidList,"删除失败，好友数量不对");
        AssertUtil.isTrue(uidList.size()==2,"删除失败，好友数量不对");

        //构建房间的key。房间key由两个uid拼接，先做升序排序uid1_uid2
        String roomKey= ChatAdapter.generateRoomKey(uidList);
        //禁用房间
        roomFriendDao.disableRoom(roomKey);
    }

    @Override
    public RoomFriend getFriendRoom(Long uid1, Long uid2) {
        String key = ChatAdapter.generateRoomKey(Arrays.asList(uid1, uid2));
        return roomFriendDao.getByKey(key);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public RoomGroup createGroupRoom(Long uid) {
        List<GroupMember> selfGroup = groupMemberDao.getSelfGroup(uid);
        AssertUtil.isEmpty(selfGroup, "每个人只能创建一个群");
        User user = userInfoCache.get(uid);
        Room room = createRoom(RoomTypeEnum.GROUP);
        //插入群
        RoomGroup roomGroup = ChatAdapter.buildGroupRoom(user, room.getId());
        roomGroupDao.save(roomGroup);
        //插入群主
        GroupMember leader = GroupMember.builder()
                .role(GroupRoleEnum.LEADER.getType())
                .groupId(roomGroup.getId())
                .uid(uid)
                .build();
        groupMemberDao.save(leader);
        return roomGroup;
    }
}
