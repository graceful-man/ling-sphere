package com.fsh.lingsp.common.chat.dao;

import cn.hutool.core.collection.CollectionUtil;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fsh.lingsp.common.chat.domain.entity.Message;
import com.fsh.lingsp.common.chat.domain.enums.MessageStatusEnum;
import com.fsh.lingsp.common.chat.domain.vo.request.ChatMessagePageReq;
import com.fsh.lingsp.common.chat.mapper.MessageMapper;
import com.fsh.lingsp.common.common.domain.vo.req.CursorPageBaseReq;
import com.fsh.lingsp.common.common.domain.vo.resp.CursorPageBaseResp;
import com.fsh.lingsp.common.common.utils.CursorUtils;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Objects;

@Service
public class MessageDao extends ServiceImpl<MessageMapper, Message> {


    /**
     * 计算 回复消息 与 当前消息 之间的间隔消息数量。
     *
     * 两个消息id之间统计数量
     *
     * @param roomId     房间 ID
     * @param replyMsgId 回复消息 id
     * @param msgId      当前消息 id
     * @return {@link Integer }
     */
    public Integer getGapCount(Long roomId, Long replyMsgId, Long msgId) {
        return lambdaQuery()
                .eq(Message::getRoomId,roomId)
                .gt(Message::getId,replyMsgId)
                .le(Message::getId,msgId)
                .count();
    }

    /**
     * 游标翻页，获取消息列表
     *
     * @param roomId    房间 ID
     * @param request   这里面除了有房间id ，还有前端传过来的上一次  游标翻页的部分参数信息。要根据这个进行翻页。
     * @param lastMsgId 上一个消息 ID ， 此为界限，只能翻 比它小的消息id。
     * @return {@link CursorPageBaseResp }<{@link Message }>
     */
    public CursorPageBaseResp<Message> getCursorPage(Long roomId, CursorPageBaseReq request, Long lastMsgId) {
        return CursorUtils.getCursorPageByMysql(this, request, wrapper -> {
            wrapper.eq(Message::getRoomId, roomId);
            wrapper.eq(Message::getStatus, MessageStatusEnum.NORMAL.getStatus());
            wrapper.le(Objects.nonNull(lastMsgId), Message::getId, lastMsgId);
        }, Message::getId);
    }

    public Integer getUnReadCount(Long roomId, Date readTime) {
        return lambdaQuery()
                .eq(Message::getRoomId, roomId)
                .gt(Objects.nonNull(readTime), Message::getCreateTime, readTime)
                .count();
    }

    /**
     * 根据房间ID逻辑删除消息
     *
     * @param roomId  房间ID
     * @param uidList 群成员列表
     * @return 是否删除成功
     */
    public Boolean removeByRoomId(Long roomId, List<Long> uidList) {
        if (CollectionUtil.isNotEmpty(uidList)) {
            LambdaUpdateWrapper<Message> wrapper = new UpdateWrapper<Message>().lambda()
                    .eq(Message::getRoomId, roomId)
                    .in(Message::getFromUid, uidList)
                    .set(Message::getStatus, MessageStatusEnum.DELETE.getStatus());
            return this.update(wrapper);
        }
        return false;
    }
}
