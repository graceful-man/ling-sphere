package com.fsh.lingsp.common.chat.domain.vo.request;

import com.fsh.lingsp.common.common.domain.vo.req.CursorPageBaseReq;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

/**
 * 作者：fsh
 * 日期：2024/03/15
 * <p>
 * 描述：
 * Description: 消息列表请求  , 继承了游标翻页
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ChatMessagePageReq extends CursorPageBaseReq {
    @NotNull
    @ApiModelProperty("会话id")
    private Long roomId;
}
