package com.fsh.lingsp.common.common.service.cache;

import java.util.List;
import java.util.Map;

/**
 * 作者：fsh
 * 日期：2024/03/10
 * <p>
 * 描述：批处理缓存框架。  顶层接口
 */
public interface BatchCache<IN, OUT> {
    /**
     * 获取单个
     */
    OUT get(IN req);

    /**
     * 获取批量
     */
    Map<IN, OUT> getBatch(List<IN> req);

    /**
     * 修改删除单个
     */
    void delete(IN req);

    /**
     * 修改删除多个
     */
    void deleteBatch(List<IN> req);
}
