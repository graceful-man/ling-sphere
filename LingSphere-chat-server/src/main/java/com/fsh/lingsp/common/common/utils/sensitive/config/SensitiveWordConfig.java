package com.fsh.lingsp.common.common.utils.sensitive.config;

import com.fsh.lingsp.common.common.utils.sensitive.DFAFilter;
import com.fsh.lingsp.common.common.utils.sensitive.MyWordFactory;
import com.fsh.lingsp.common.common.utils.sensitive.SensitiveWordBs;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 作者：fsh
 * 日期：2024/03/16
 * <p>
 * 描述：敏感词配置类
 */
@Configuration
public class SensitiveWordConfig {

    @Autowired
    private MyWordFactory myWordFactory;

    /**
     * 初始化引导类 。
     *
     *
     * @return 初始化引导类
     * @since 1.0.0
     */
    @Bean
    public SensitiveWordBs sensitiveWordBs() {
        return SensitiveWordBs.newInstance()
                .filterStrategy(DFAFilter.getInstance())//默认是DFA过滤
                .sensitiveWord(myWordFactory)//加载过滤词，这里只是把类传过去了。调用它的getWordList()即可拿到过滤词
                .init();//初始化
    }

}