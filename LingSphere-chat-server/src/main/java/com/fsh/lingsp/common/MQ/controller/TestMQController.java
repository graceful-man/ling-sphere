package com.fsh.lingsp.common.MQ.controller;

import com.abin.mallchat.transaction.service.MQProducer;
import com.fsh.lingsp.common.chat.dao.MessageDao;
import com.fsh.lingsp.common.chat.domain.entity.Message;
import com.fsh.lingsp.common.common.domain.vo.resp.ApiResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 作者：fsh
 * 日期：2024/03/11
 * <p>
 * 描述：测试 MQConnect
 */
@RestController
@Api(tags = "MQ相关测试")
public class TestMQController {

    @Resource
    private RocketMQTemplate rocketMQTemplate;
    @Resource
    private MQProducer mqProducer;
    @Autowired
    private MessageDao messageDao;

    /**
     * 模拟发送一条MQ消息
     */
    @GetMapping("/sendMQ")
    @ApiOperation("模拟发送一条MQ消息")
    public void sendMQ() {
        org.springframework.messaging.Message<String> build = MessageBuilder.withPayload("123").build();
        rocketMQTemplate.send("test-topic", build);
    }

    /**
     * 模拟发送一条MQ消息
     */
    @PostMapping("/secureInvoke")
    @ApiOperation("本地消息表测试")
    @Transactional
    public void secureInvoke(String msg) {
        Message message=Message.builder()
                .content(msg)
                .fromUid(11L)
                .type(1)
                .roomId(1L)
                .status(0).build();
        messageDao.save(message);
        mqProducer.sendSecureMsg("test_topic",msg,msg);
    }

}
