package com.fsh.lingsp.common.user.mapper;

import com.fsh.lingsp.common.user.domain.entity.UserFriend;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户联系人表 Mapper 接口
 * </p>
 *
 * @author ${author}
 * @since 2024-03-08
 */
public interface UserFriendMapper extends BaseMapper<UserFriend> {

}
