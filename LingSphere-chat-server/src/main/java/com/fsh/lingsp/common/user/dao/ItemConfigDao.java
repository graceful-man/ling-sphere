package com.fsh.lingsp.common.user.dao;

import com.fsh.lingsp.common.user.domain.entity.ItemConfig;
import com.fsh.lingsp.common.user.mapper.ItemConfigMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 功能物品配置表 服务实现类
 * </p>
 *
 * @author ${author}
 * @since 2024-02-27
 */
@Service
public class ItemConfigDao extends ServiceImpl<ItemConfigMapper, ItemConfig> {

    /**
     * 1.查询出所有的徽章；
     */
    public List<ItemConfig> getBadgeByType(Integer type) {
        return lambdaQuery()
                .eq(ItemConfig::getType,type)
                .list();
    }
}
