package com.fsh.lingsp.common.user.service.adapter;


import com.fsh.lingsp.common.user.domain.entity.User;
import com.fsh.lingsp.common.user.domain.entity.UserApply;
import com.fsh.lingsp.common.user.domain.entity.UserFriend;
import com.fsh.lingsp.common.user.domain.enums.ApplyReadStatusEnum;
import com.fsh.lingsp.common.user.domain.enums.ApplyStatusEnum;
import com.fsh.lingsp.common.user.domain.enums.ApplyTypeEnum;
import com.fsh.lingsp.common.user.domain.vo.req.friend.FriendApplyReq;
import com.fsh.lingsp.common.user.domain.vo.resp.friend.FriendApplyResp;
import com.fsh.lingsp.common.user.domain.vo.resp.friend.FriendResp;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * 作者：fsh
 * 日期：2024/03/08
 * <p>
 * 描述：
 * Description: 好友适配器
 */
public class FriendAdapter {

    /**
     *  FriendResp : uid 和 在线状态。
     *
     *  第一个参数我觉得挺多余的。
     */
    public static List<FriendResp> buildFriend(List<UserFriend> list, List<User> userList) {
        //构建map ， key为uid  value为User
        Map<Long, User> userMap = userList.stream().collect(Collectors
                .toMap(User::getId, user -> user));
        return list.stream().map(userFriend -> {
            FriendResp resp=new FriendResp();
            resp.setUid(userFriend.getFriendUid());
            User user = userMap.get(userFriend.getFriendUid());
            if (Objects.nonNull(user)){
                resp.setActiveStatus(user.getActiveStatus());
            }
            return resp;
        }).collect(Collectors.toList());
    }

    /**
     * 构建申请人列表
     */
    public static List<FriendApplyResp> buildFriendApplyList(List<UserApply> records) {
        return records.stream().map(userApply -> {
           FriendApplyResp resp=FriendApplyResp.builder()
                   .applyId(userApply.getId())
                   .uid(userApply.getUid())
                   .type(userApply.getType())
                   .msg(userApply.getMsg())
                   .status(userApply.getStatus()).build();
           return resp;
        }).collect(Collectors.toList());
    }

    /**
     * 构建 申请记录
     */
    public static UserApply buildFriendApply(Long uid, FriendApplyReq req) {
        UserApply userApply= UserApply.builder()
                .uid(uid)
                .type(ApplyTypeEnum.ADD_FRIEND.getCode())
                .targetId(req.getTargetUid())
                .msg(req.getMsg())
                .status(ApplyStatusEnum.WAIT_APPROVAL.getStatus())
                .readStatus(ApplyReadStatusEnum.UNREAD.getCode()).build();
        return  userApply;
    }
}
