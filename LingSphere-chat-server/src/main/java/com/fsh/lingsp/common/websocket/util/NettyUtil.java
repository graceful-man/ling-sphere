package com.fsh.lingsp.common.websocket.util;

import io.netty.channel.Channel;
import io.netty.util.Attribute;
import io.netty.util.AttributeKey;


/**
 * 作者：fsh
 * 日期：2024/02/26
 * <p>
 * 描述：定义一个公共的静态成员变量TOKEN，类型为AttributeKey<String>，
 */
public class NettyUtil {

    public static final AttributeKey<String> IP = AttributeKey.valueOf("ip");
    public static AttributeKey<Long> UID = AttributeKey.valueOf("uid");
    // 通过调用AttributeKey的静态方法valueOf并传入字符串"token"来初始化。
    public static AttributeKey<String> TOKEN = AttributeKey.valueOf("token");

    /**
     * 定义一个公共的静态泛型方法setAttr，
     * 接受三个参数：一个Channel对象、一个AttributeKey<T>对象和一个泛型参数T的数据。
     */
    public static <T> void setAttr(Channel channel, AttributeKey<T> attributeKey, T value) {
        // 从Channel对象中获取与attributeKey关联的Attribute<T>对象。
        Attribute<T> attr = channel.attr(attributeKey);
        // 使用Attribute<T>对象的set方法，将传入的数据data设置到该属性中。
        attr.set(value);
    }

    /**
     *  定义一个公共的静态泛型方法getAttr，接受两个参数：一个Channel对象和一个AttributeKey<T>对象ip。
     *  返回值为泛型参数T的类型。
     */
    public static <T> T getAttr(Channel channel, AttributeKey<T> key) {
        // 从Channel对象中获取与ip关联的Attribute<T>对象.
        Attribute<T> attr = channel.attr(key);
        // get方法会返回该属性中存储的数据，类型为T。
        return attr.get();
    }
}
