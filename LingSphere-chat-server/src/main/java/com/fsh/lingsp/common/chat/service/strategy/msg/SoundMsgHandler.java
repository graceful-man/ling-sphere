package com.fsh.lingsp.common.chat.service.strategy.msg;

import com.fsh.lingsp.common.chat.dao.MessageDao;
import com.fsh.lingsp.common.chat.domain.entity.Message;
import com.fsh.lingsp.common.chat.domain.entity.msg.MessageExtra;
import com.fsh.lingsp.common.chat.domain.entity.msg.SoundMsgDTO;
import com.fsh.lingsp.common.chat.domain.enums.MessageTypeEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

/**
 * 作者：fsh
 * 日期：2024/03/10
 * <p>
 * 描述：
 * Description:语音消息
 */
@Component
public class SoundMsgHandler extends AbstractMsgHandler<SoundMsgDTO> {
    @Autowired
    private MessageDao messageDao;

    @Override
    MessageTypeEnum getMsgTypeEnum() {
        return MessageTypeEnum.SOUND;
    }

    @Override
    protected void checkMsg(SoundMsgDTO body, Long roomId, Long uid) {
        super.checkMsg(body, roomId, uid);
    }

    @Override
    public void saveMsg(Message msg, SoundMsgDTO body) {
        MessageExtra extra = Optional.ofNullable(msg.getExtra()).orElse(new MessageExtra());
        extra.setSoundMsgDTO(body);
        Message update = new Message();
        update.setId(msg.getId());
        update.setExtra(extra);
        messageDao.updateById(update);
    }

    @Override
    public Object showMsg(Message msg) {
        return msg.getExtra().getSoundMsgDTO();
    }

    @Override
    public Object showReplyMsg(Message msg) {
        return "语音";
    }

    @Override
    public String showContactMsg(Message msg) {
        return "[语音]";
    }
}
