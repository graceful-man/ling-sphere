package com.fsh.lingsp.common.chat.domain.vo.request;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;


/**
 * 作者：fsh
 * 日期：2024/03/10
 * <p>
 * 描述：
 * 聊天信息点播
 * Description: 消息发送请求体
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ChatMessageReq {
    @NotNull
    @ApiModelProperty("房间id")
    private Long roomId;

    /**
     * @see com.fsh.lingsp.common.chat.domain.enums.MessageTypeEnum
     */
    @ApiModelProperty("消息类型")
    @NotNull
    private Integer msgType;

    /**
     * @see com.fsh.lingsp.common.chat.domain.entity.msg
     */
    @ApiModelProperty("消息内容，类型不同传值不同，见https://www.yuque.com/snab/mallcaht/rkb2uz5k1qqdmcmd")
    @NotNull
    private Object body;

}
