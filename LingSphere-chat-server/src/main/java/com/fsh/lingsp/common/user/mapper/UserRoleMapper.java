package com.fsh.lingsp.common.user.mapper;

import com.fsh.lingsp.common.user.domain.entity.UserRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户角色关系表 Mapper 接口
 * </p>
 *
 * @author ${author}
 * @since 2024-03-06
 */
public interface UserRoleMapper extends BaseMapper<UserRole> {

}
