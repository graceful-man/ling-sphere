package com.fsh.lingsp.common.user.service.handler;

import com.fsh.lingsp.common.user.service.WXMsgService;
import com.fsh.lingsp.common.user.service.adapter.TextBuilder;
import lombok.extern.slf4j.Slf4j;
import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.common.session.WxSessionManager;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.message.WxMpXmlMessage;
import me.chanjar.weixin.mp.bean.message.WxMpXmlOutMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.net.URLEncoder;
import java.util.Map;

/**
 * 作者：fsh
 * 日期：2024/02/23
 * <p>
 * 描述：已关注用户的扫码处理程序
 */
@Component
@Slf4j
public class ScanHandler extends AbstractHandler {

    @Autowired
    private WXMsgService wxMsgService;

    @Override
    public WxMpXmlOutMessage handle(WxMpXmlMessage wxMpXmlMessage
            , Map<String, Object> map
            , WxMpService wxMpService
            , WxSessionManager wxSessionManager) throws WxErrorException {

        if (log.isInfoEnabled()){
            log.info("ScanHandler.handle==>{}","扫码了");
        }
        return wxMsgService.scan(wxMpXmlMessage,wxMpService);
    }
}
