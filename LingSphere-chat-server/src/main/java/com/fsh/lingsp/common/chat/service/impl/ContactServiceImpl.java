package com.fsh.lingsp.common.chat.service.impl;

import com.fsh.lingsp.common.chat.dao.ContactDao;
import com.fsh.lingsp.common.chat.dao.MessageDao;
import com.fsh.lingsp.common.chat.domain.dto.MsgReadInfoDTO;
import com.fsh.lingsp.common.chat.domain.entity.Contact;
import com.fsh.lingsp.common.chat.domain.entity.Message;
import com.fsh.lingsp.common.chat.service.ContactService;
import com.fsh.lingsp.common.common.utils.AssertUtil;
import com.fsh.lingsp.common.user.service.adapter.ChatAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Description: 会话列表
 * Author: <a href="https://github.com/zongzibinbin">abin</a>
 * Date: 2023-07-22
 */
@Service
public class ContactServiceImpl implements ContactService {

    @Autowired
    private ContactDao contactDao;
    @Autowired
    private MessageDao messageDao;

    @Override
    public Contact createContact(Long uid, Long roomId) {
        Contact contact = contactDao.get(uid, roomId);
        if (Objects.isNull(contact)) {
            contact = ChatAdapter.buildContact(uid, roomId);
            contactDao.save(contact);
        }
        return contact;
    }

    @Override
    public Integer getMsgReadCount(Message message) {
        return contactDao.getReadCount(message);
    }

    @Override
    public Integer getMsgUnReadCount(Message message) {
        return contactDao.getUnReadCount(message);
    }

    /**
     * @param messages 消息
     */
    @Override
    public Map<Long, MsgReadInfoDTO> getMsgReadInfo(List<Message> messages) {
        Map<Long, List<Message>> roomGroup = messages.stream().collect(Collectors.groupingBy(Message::getRoomId));
        // 所以实际上 上面的消息是同一个房间中的
        AssertUtil.equal(roomGroup.size(), 1, "只能查相同房间下的消息");

        Long roomId = roomGroup.keySet().iterator().next();
        // 计算房间内人的总人数
        Integer totalCount = contactDao.getTotalCount(roomId);
        // 对这些消息遍历，一个一个比对房间内的人的  read_time字段
        return messages.stream().map(message -> {
            // 这个MsgReadInfoDTO 包含 消息id、已读数和未读数。
            MsgReadInfoDTO readInfoDTO = new MsgReadInfoDTO();
            readInfoDTO.setMsgId(message.getId());
            // 查询出已读数 （就是拿 消息的发送时间和房间内人的 read_time作对比。 read_time > 消息时间，就是已读）
            Integer readCount = contactDao.getReadCount(message);
            readInfoDTO.setReadCount(readCount);
            // 未读数就是  全部-已读数
            readInfoDTO.setUnReadCount(totalCount - readCount - 1);
            return readInfoDTO;
        }).collect(Collectors.toMap(MsgReadInfoDTO::getMsgId, Function.identity()));
        //收集起来 ，key为消息id，value为 MsgReadInfoDTO对象
    }
}
