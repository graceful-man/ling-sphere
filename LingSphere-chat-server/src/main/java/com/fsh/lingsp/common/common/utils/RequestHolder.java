package com.fsh.lingsp.common.common.utils;

import com.fsh.lingsp.common.common.domain.dto.RequestInfo;

/**
 * 作者：fsh
 * 日期：2024/03/01
 * <p>
 * 描述：threadLocal
 *
 *  todo 改进 ，参考fsh-club
 */
public class RequestHolder {
    private static final ThreadLocal<RequestInfo> threadLocal=new ThreadLocal<>();

    public static void set(RequestInfo requestInfo){
        threadLocal.set(requestInfo);
    }

    public static RequestInfo get(){
        return threadLocal.get();
    }

    public static void remove(){
        threadLocal.remove();
    }
}
