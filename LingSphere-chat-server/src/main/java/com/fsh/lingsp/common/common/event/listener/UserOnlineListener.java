package com.fsh.lingsp.common.common.event.listener;


import com.fsh.lingsp.common.common.event.UserOnlineEvent;
import com.fsh.lingsp.common.user.dao.UserDao;
import com.fsh.lingsp.common.user.domain.entity.User;
import com.fsh.lingsp.common.user.domain.enums.UserActiveStatusEnum;
import com.fsh.lingsp.common.user.service.IpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.transaction.event.TransactionPhase;
import org.springframework.transaction.event.TransactionalEventListener;

/**
 * 作者：fsh
 * 日期：2024/03/05
 * <p>
 * 描述：用户上线监听器
 */
@Component
public class UserOnlineListener{
    @Autowired
    private UserDao userDao;
    @Autowired
    private IpService ipService;

    /**
     * 保存用户ip。 解析ip归属地
     *
     * fallbackExecution = true。 这个参数的意思是，如果没有事务，那也提交。
     *      刚好，我们监听的这个事件没有处在事务中。
     */
    @Async
    @TransactionalEventListener(classes = UserOnlineEvent.class
            ,phase = TransactionPhase.AFTER_COMMIT
            ,fallbackExecution = true)
    public void saveUserIpinfo(UserOnlineEvent event){
        User user = event.getUser();
        User update=User.builder()
                .id(user.getId())
                .lastOptTime(user.getLastOptTime())
                .ipInfo(user.getIpInfo())
                .activeStatus(UserActiveStatusEnum.ONLINE.getStatus()).build();
        userDao.updateById(update);
        //用户ip详情的解析
        ipService.refreshDetailAsync(user.getId());
    }
}
