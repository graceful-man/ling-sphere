package com.fsh.lingsp.common.chat.dao;

import com.fsh.lingsp.common.chat.mapper.RoomFriendMapper;
import com.fsh.lingsp.common.common.domain.enums.NormalOrNoEnum;
import com.fsh.lingsp.common.chat.domain.entity.RoomFriend;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 单聊房间表 服务实现类
 * </p>
 *
 * @author ${author}
 * @since 2024-03-08
 */
@Service
public class RoomFriendDao extends ServiceImpl<RoomFriendMapper, RoomFriend>  {

    /**
     * 根据 roomKey 查询 房间
     */
    public RoomFriend getByRoomKey(String roomKey) {
        return lambdaQuery()
                .eq(RoomFriend::getRoomKey,roomKey)
                .one();
    }

    /**
     * 恢复房间(聊天会话的窗口)。
     */
    public void restoreRoom(Long id) {
        lambdaUpdate()
                .eq(RoomFriend::getId,id)
                .set(RoomFriend::getStatus, NormalOrNoEnum.NORMAL.getStatus())
                .update();
    }

    /**
     * 禁用房间
     */
    public void disableRoom(String roomKey) {
        lambdaUpdate()
                .eq(RoomFriend::getRoomKey, roomKey)
                .set(RoomFriend::getStatus, NormalOrNoEnum.NOT_NORMAL.getStatus())
                .update();
    }

    /**
     * 根据 roomId 查询 roomFriend表
     * @param roomId 房间 ID
     */
    public RoomFriend getByRoomId(Long roomId) {
        return lambdaQuery()
                .eq(RoomFriend::getRoomId,roomId)
                .one();
    }

    public List<RoomFriend> listByRoomIds(List<Long> roomIds) {
        return lambdaQuery()
                .in(RoomFriend::getRoomId, roomIds)
                .list();
    }

    public RoomFriend getByKey(String key) {
        return lambdaQuery().eq(RoomFriend::getRoomKey, key).one();
    }

}
