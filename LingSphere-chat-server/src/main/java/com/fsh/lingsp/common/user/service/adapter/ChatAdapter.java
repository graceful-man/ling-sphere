package com.fsh.lingsp.common.user.service.adapter;

import com.fsh.lingsp.common.chat.domain.entity.Contact;
import com.fsh.lingsp.common.chat.domain.entity.RoomGroup;
import com.fsh.lingsp.common.common.domain.enums.HotFlagEnum;
import com.fsh.lingsp.common.common.domain.enums.NormalOrNoEnum;
import com.fsh.lingsp.common.chat.domain.entity.Room;
import com.fsh.lingsp.common.chat.domain.entity.RoomFriend;
import com.fsh.lingsp.common.user.domain.entity.User;
import com.fsh.lingsp.common.user.domain.enums.RoomTypeEnum;

import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 作者：fsh
 * 日期：2024/03/09
 * <p>
 * 描述：聊天适配器
 */
public class ChatAdapter {

    //逗号分隔符
    public static final String SEPARATOR = ",";

    /**
     * 创建一个会话窗口的 roomKey。   两个人的房间。
     *
     * 本质：就是把list中的 两个uid升序排序，再拼成一个字符串。
     * 比如：
     * 1.   6 ，5   -》key="5,6"
     * 2.   4 , 7   -> key="4,7"
     */
    public static String generateRoomKey(List<Long> uidList) {
        return uidList.stream()
                .sorted()
                .map(String::valueOf)
                .collect(Collectors.joining(SEPARATOR));
    }

    /**
     * 构建一个Room
     */
    public static Room buildRoom(RoomTypeEnum roomTypeEnum) {
        Room room = new Room();
        room.setType(roomTypeEnum.getType());
        room.setHotFlag(HotFlagEnum.NOT.getType());
        return room;
    }

    /**
     * 构建一个roomFriend。  这里存的是 两个人的uid，roomKey，room的id ，等关联信息
     */
    public static RoomFriend buildFriendRoom(Long roomId, List<Long> uidList) {
        //先让两个uid排序，从小到大。
        List<Long> collect = uidList.stream().sorted().collect(Collectors.toList());
        RoomFriend roomFriend = new RoomFriend();
        roomFriend.setRoomId(roomId);
        //uid1 < uid2
        roomFriend.setUid1(collect.get(0));
        roomFriend.setUid2(collect.get(1));
        roomFriend.setRoomKey(generateRoomKey(uidList));
        roomFriend.setStatus(NormalOrNoEnum.NORMAL.getStatus());
        return roomFriend;
    }

    public static RoomGroup buildGroupRoom(User user, Long roomId) {
        RoomGroup roomGroup = new RoomGroup();
        roomGroup.setName(user.getName() + "的群组");
        roomGroup.setAvatar(user.getAvatar());
        roomGroup.setRoomId(roomId);
        return roomGroup;
    }

    public static Set<Long> getFriendUidSet(Collection<RoomFriend> values, Long uid) {
        return values.stream()
                .map(a -> getFriendUid(a, uid))
                .collect(Collectors.toSet());
    }

    /**
     * 获取好友uid
     */
    public static Long getFriendUid(RoomFriend roomFriend, Long uid) {
        return Objects.equals(uid, roomFriend.getUid1()) ? roomFriend.getUid2() : roomFriend.getUid1();
    }

    public static Contact buildContact(Long uid, Long roomId) {
        Contact contact = new Contact();
        contact.setRoomId(roomId);
        contact.setUid(uid);
        return contact;
    }
}
