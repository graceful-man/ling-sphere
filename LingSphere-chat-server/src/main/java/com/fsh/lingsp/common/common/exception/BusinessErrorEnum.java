package com.fsh.lingsp.common.common.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 作者：fsh
 * 日期：2024/03/01
 * <p>
 * 描述：
 * Description: 自定义业务异常码
 */
@AllArgsConstructor
@Getter
public enum BusinessErrorEnum implements ErrorEnum {
    //==================================common==================================
    BUSINESS_ERROR(1001, "{0}"),
    //==================================user==================================
    //==================================chat==================================
    SYSTEM_ERROR(1001, "系统出小差了，请稍后再试哦~~"),
    ;
    private Integer code;
    private String msg;

    @Override
    public Integer getErrorCode() {
        return code;
    }

    @Override
    public String getErrorMsg() {
        return msg;
    }
}
