package com.fsh.lingsp.common.user.consumer;

import com.fsh.lingsp.common.common.constant.MQConstant;
import com.fsh.lingsp.common.common.domain.dto.PushMessageDTO;
import com.fsh.lingsp.common.user.domain.enums.WSPushTypeEnum;
import com.fsh.lingsp.common.websocket.service.WebSocketService;
import org.apache.rocketmq.spring.annotation.MessageModel;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 作者：fsh
 * 日期：2024/03/14
 * <p>
 * 描述：
 * Description:
 *
 * topic 为 "websocket_push"; 监听消息 ，将消息写给每个人的收件箱。
 */
@RocketMQMessageListener(topic = MQConstant.PUSH_TOPIC, consumerGroup = MQConstant.PUSH_GROUP, messageModel = MessageModel.BROADCASTING)
@Component
public class PushConsumer implements RocketMQListener<PushMessageDTO> {
    @Autowired
    private WebSocketService webSocketService;

    @Override
    public void onMessage(PushMessageDTO message) {
        WSPushTypeEnum wsPushTypeEnum = WSPushTypeEnum.of(message.getPushType());
        switch (wsPushTypeEnum) {
            case USER: //这个就是针对  单聊和普通群聊 ，将消息写给 群内所有人的 的收件箱。
                message.getUidList().forEach(uid -> {
                    webSocketService.sendToUid(message.getWsBaseMsg(), uid);
                });
                break;
            case ALL: // 这个就是热点群聊 ，写给所有在线的用户
                webSocketService.sendToAllOnline(message.getWsBaseMsg(), null);
                break;
        }
    }
}
