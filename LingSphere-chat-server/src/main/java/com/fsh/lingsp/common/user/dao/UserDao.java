package com.fsh.lingsp.common.user.dao;

import cn.hutool.core.collection.CollectionUtil;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.fsh.lingsp.common.common.domain.enums.NormalOrNoEnum;
import com.fsh.lingsp.common.common.domain.enums.YseOrNoEnum;
import com.fsh.lingsp.common.common.domain.vo.req.CursorPageBaseReq;
import com.fsh.lingsp.common.common.domain.vo.resp.CursorPageBaseResp;
import com.fsh.lingsp.common.common.utils.CursorUtils;
import com.fsh.lingsp.common.user.domain.entity.User;
import com.fsh.lingsp.common.user.domain.enums.ChatActiveStatusEnum;
import com.fsh.lingsp.common.user.mapper.UserMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 用户表 服务实现类
 * </p>
 *
 * @author ${author}
 * @since 2024-02-23
 *
 * UserDao就是UserServiceImpl
 */
@Service
public class UserDao extends ServiceImpl<UserMapper, User> {

    /**
     * 根据openId查询用户
     */
    public User getByOpenId(String openId) {
        return lambdaQuery()
                .eq(User::getOpenId,openId)
                .one();
    }

    /**
     * 根据名字查询用户
     */
    public User getByName(String name) {
        return lambdaQuery()
                .eq(User::getName,name)
                .one();
    }


    /**
     * 改名
     */
    public void modifyName(Long uid, String name) {
        lambdaUpdate()
                .eq(User::getId,uid)
                .set(User::getName,name)
                .update();
    }

    /**
     * 更新用户佩戴的徽章
     */
    public void wearingBadge(Long uid, Long itemId) {
        lambdaUpdate()
                .eq(User::getId,uid)
                .set(User::getItemId,itemId)
                .update();
    }

    /**
     * 修改用户的status .  0正常 1拉黑
     */
    public void changeUserStatus(Long uid) {
        lambdaUpdate()
                .eq(User::getId,uid)
                .set(User::getStatus,YseOrNoEnum.YES.getStatus())
                .update();
    }

    /**
     * 通过好友uid查询他们的在线信息
     */
    public List<User> getFriendList(List<Long> friendUids) {
        return lambdaQuery()
                .in(User::getId,friendUids)
                .select(User::getId,User::getActiveStatus,User::getName,User::getAvatar)
                .list();
    }

    public Integer getOnlineCount(List<Long> memberUidList) {
        return lambdaQuery()
                .eq(User::getActiveStatus, ChatActiveStatusEnum.ONLINE.getStatus())
                .in(CollectionUtil.isNotEmpty(memberUidList), User::getId, memberUidList)
                .count();
    }

    public CursorPageBaseResp<User> getCursorPage(List<Long> memberUidList, CursorPageBaseReq request, ChatActiveStatusEnum online) {
        return CursorUtils.getCursorPageByMysql(this, request, wrapper -> {
            wrapper.eq(User::getActiveStatus, online.getStatus());//筛选上线或者离线的
            wrapper.in(CollectionUtils.isNotEmpty(memberUidList), User::getId, memberUidList);//普通群对uid列表做限制
        }, User::getLastOptTime);
    }

    public List<User> getMemberList() {
        return lambdaQuery()
                .eq(User::getStatus, NormalOrNoEnum.NORMAL.getStatus())
                .orderByDesc(User::getLastOptTime)//最近活跃的1000个人，可以用lastOptTime字段，但是该字段没索引，updateTime可平替
                .last("limit 1000")//毕竟是大群聊，人数需要做个限制
                .select(User::getId, User::getName, User::getAvatar)
                .list();

    }
}
