package com.fsh.lingsp.common.user.domain.vo.resp.friend;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * 作者：fsh
 * 日期：2024/03/08
 * <p>
 * 描述：联系人列表 响应体
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class FriendResp {

    @ApiModelProperty("好友uid")
    private Long uid;
    /**
     *
     */
    @ApiModelProperty("在线状态 1在线 2离线")
    private Integer activeStatus;
}
