package com.fsh.lingsp.common.common.utils.discover;

//import org.jetbrains.annotations.Nullable;
import org.jsoup.nodes.Document;
import org.springframework.lang.Nullable;

/**
 * 作者：fsh
 * 日期：2024/03/15
 * <p>
 * 描述：
 * Description: 针对微信公众号文章的标题获取类
 */
public class WxUrlDiscover extends AbstractUrlDiscover {


    /**
     *     // 重写父类中的getTitle方法，用于从Document对象中获取标题
     *     // 使用Open Graph协议中的og:title属性来获取标题
     *
     * @param document 网页DOM对象
     * @return {@link String }
     */
    @Nullable
    @Override
    public String getTitle(Document document) {
        // 查找所有属性为"property"，且值为"og:title"的元素，并获取其"content"属性值作为标题
        return document.getElementsByAttributeValue("property", "og:title").attr("content");
    }


    /**
     *     // 重写父类中的getDescription方法，用于从Document对象中获取描述
     *     // 使用Open Graph协议中的og:description属性来获取描述
     *
     * @param document 网页DOM对象
     * @return {@link String }
     */
    @Nullable
    @Override
    public String getDescription(Document document) {
        // 查找所有属性为"property"，且值为"og:description"的元素，并获取其"content"属性值作为描述
        return document.getElementsByAttributeValue("property", "og:description").attr("content");
    }


    /**
     *     // 重写父类中的getImage方法，用于从给定的url和Document对象中获取图片链接
     *     // 使用Open Graph协议中的og:image属性来获取图片链接
     *
     * @param url      网址
     * @param document 网页DOM对象
     * @return {@link String }
     */
    @Nullable
    @Override
    public String getImage(String url, Document document) {
        // 查找所有属性为"property"，且值为"og:image"的元素，并获取其"content"属性值作为图片链接
        String href = document.getElementsByAttributeValue("property", "og:image").attr("content");

        // 尝试连接图片链接，如果连接成功则返回链接，否则返回null
        return isConnect(href) ? href : null;
    }
}
