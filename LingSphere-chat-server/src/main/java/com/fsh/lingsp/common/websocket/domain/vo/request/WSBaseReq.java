package com.fsh.lingsp.common.websocket.domain.vo.request;

import lombok.Data;

/**
 * 作者：fsh
 * 日期：2024/02/22
 * <p>
 * 描述：ws的基本请求信息体
 */
@Data
public class WSBaseReq {
    /**
     * 请求类型 1.请求登录二维码，2心跳检测
     *
     * 这个注解可以直接跳到枚举类，方便查看
     * @see com.fsh.lingsp.common.websocket.domain.enums.WSReqTypeEnum
     */
    private Integer type;

    /**
     * 每个请求包具体的数据，类型不同结果不同
     */
    private String data;
}
