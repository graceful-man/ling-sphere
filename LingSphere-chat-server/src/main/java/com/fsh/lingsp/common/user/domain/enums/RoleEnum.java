package com.fsh.lingsp.common.user.domain.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * 作者：fsh
 * 日期：2024/03/06
 *
 * 角色枚举
 */
@AllArgsConstructor
@Getter
public enum RoleEnum {
    //超管有所有权限（拉黑+语句撤回）
    ADMIN(1L, "超级管理员"),
    //只有群聊的撤回功能
    CHAT_MANGER(2L, "语界群聊管理员"),
    ;

    private Long id;
    private final String desc;

    private static Map<Long, RoleEnum> cache;

    static {
        cache = Arrays.stream(RoleEnum.values()).collect(Collectors.toMap(RoleEnum::getId, Function.identity()));
    }

    public static RoleEnum of(Integer type) {
        return cache.get(type);
    }
}
