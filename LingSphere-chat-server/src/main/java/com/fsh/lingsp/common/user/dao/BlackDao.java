package com.fsh.lingsp.common.user.dao;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fsh.lingsp.common.user.domain.entity.Black;
import com.fsh.lingsp.common.user.domain.entity.ItemConfig;
import com.fsh.lingsp.common.user.domain.vo.req.user.BlackReq;
import com.fsh.lingsp.common.user.mapper.BlackMapper;
import com.fsh.lingsp.common.user.mapper.ItemConfigMapper;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 功能物品配置表 服务实现类
 * </p>
 *
 * @author ${author}
 * @since 2024-02-27
 */
@Service
public class BlackDao extends ServiceImpl<BlackMapper, Black> {

}
