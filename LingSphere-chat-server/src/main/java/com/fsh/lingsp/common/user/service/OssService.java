package com.fsh.lingsp.common.user.service;


import com.abin.mallchat.oss.domain.OssResp;
import com.fsh.lingsp.common.user.domain.vo.req.oss.UploadUrlReq;

import java.util.List;

/**
 * <p>
 * oss 服务类
 * </p>
 *
 * @author <a href="https://github.com/zongzibinbin">abin</a>
 * @since 2023-03-19
 */
public interface OssService {

    /**
     * 1.上传文件
     * 2.返回临时的上传链接
     */
    OssResp getUploadUrl(Long uid, UploadUrlReq req);

    /**
     * 获取所有桶的名字
     */
    List<String> getAllBucket();
}
