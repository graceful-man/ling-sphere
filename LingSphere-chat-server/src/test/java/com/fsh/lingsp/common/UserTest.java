package com.fsh.lingsp.common;

import com.fsh.lingsp.common.user.dao.UserDao;
import com.fsh.lingsp.common.user.domain.entity.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;

@SpringBootTest
@RunWith(SpringRunner.class)
public class UserTest {

    @Autowired
    private UserDao userDao;

    @Test
    public void testInsert(){
        User user = new User();
        user.setOpenId("123");
        user.setLastOptTime(new Date());
        userDao.save(user);
    }
}
